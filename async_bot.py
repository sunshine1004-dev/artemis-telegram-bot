import os
import asyncio
from dotenv import load_dotenv
from telebot.async_telebot import AsyncTeleBot
from telebot import types
from telebot import formatting

load_dotenv()
BOT_TOKEN = os.environ.get('TELE_BOT_TOKEN')

bot = AsyncTeleBot(BOT_TOKEN)


# Handle '/start' and '/help'
@bot.message_handler(commands=['help', 'start'])
async def start(message: types.Message):
    text = 'Hi, I am Artemis Signal Bot.\nYou can receive the news signals and trade the related tokens from there!'
    await bot.send_message(message.chat.id, f'Your telegram chat ID is {message.chat.id}')


asyncio.run(bot.polling())