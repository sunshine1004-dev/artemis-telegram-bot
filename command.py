#!/usr/bin/env python
# pylint: disable=unused-argument
# This program is dedicated to the public domain under the CC0 license.

"""Simple inline keyboard bot with multiple CallbackQueryHandlers.

This Bot uses the Application class to handle the bot.
First, a few callback functions are defined as callback query handler. Then, those functions are
passed to the Application and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Example of a bot that uses inline keyboard that has multiple CallbackQueryHandlers arranged in a
ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line to stop the bot.
"""
import logging

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, ForceReply
from telegram.ext import (
    Application,
    CallbackQueryHandler,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    filters,
    MessageHandler
)
import os
from dotenv import load_dotenv
from utils.models import Users, UserSettingsGlobal, ApiKey, SessionLocal
import ccxt.async_support as ccxt
from ccxt import Exchange
import socketio
import asyncio
import multiprocessing

sio = socketio.Client()
load_dotenv()

BOT_TOKEN = os.environ.get('TELE_BOT_TOKEN')
session = SessionLocal()
application = Application.builder().token(BOT_TOKEN).build()
updater = application.updater
contexter = application.context_types

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)

# Stages
START_ROUTES, CEX_ROUTES, END_ROUTES = range(3)

cex_list1 = [
    {
        "title": "Binance",
        "slug": "binance",
        "required_passphrase": False
    },
    {
        "title": "Kucoinfutures",
        "slug": "kucoinfutures",
        "required_passphrase": True
    },
    {
        "title": "Kucoin",
        "slug": "kucoin",
        "required_passphrase": True
    },
    {
        "title": "Bybit",
        "slug": "bybit",
        "required_passphrase": False
    }
]

cex_list2 = [
    {
        "title": "Bitget",
        "slug": "bitget",
        "required_passphrase": False
    },
    {
        "title": "GateIO",
        "slug": "gateio",
        "required_passphrase": False
    },
    {
        "title": "OKX",
        "slug": "okx",
        "required_passphrase": False
    },
    {
        "title": "WOO X",
        "slug": "woo",
        "required_passphrase": False
    }
]

messages = {}
users = []

@sio.on("signals")
def on_message(data):
    asyncio.run(handle_message(data))

@sio.on('connect')
def connect_handler():
    print('Connected to socket server!')

@sio.on('disconnect')
def disconnect_handler():
    print('Disconnected to socket server!')

async def handle_message(data):
    user_loops = [send_signal(user, data) for user in users]
    await asyncio.gather(*user_loops)

async def send_signal(chat_id, signal) -> int:
    user_info = get_user_info_by_chat_id(chat_id)
    if not user_info or not user_info.public_id:
        return CEX_ROUTES
    default_exchange = get_default_exchange_by_user_id(user_info.public_id)
    # if default_exchange:
    #     await updater.bot.send_message(chat_id=chat_id, text=f"Your default exchange is {default_exchange.exchange.title()}({default_exchange.account_name}). You can handle your exchange API keys 👉 /exchanges")
    # else :
    #     await updater.bot.send_message(chat_id=chat_id, text="You haven't set a default exchange. Please set the default to buy or sell tokens 👉 /exchanges")
    if "title" in signal:
        await updater.bot.send_message(chat_id=chat_id, text=signal["title"])
    if "tokens" in signal and signal["tokens"]:
        for token in signal["tokens"]:
            pair = token["pair"]
            symbol = token["symbol"]
            price = token["priceFound"]
            unit = pair.split("/")[1]
            keyboard = [
                [
                    InlineKeyboardButton(f"Buy {symbol}", callback_data=f"buy_token_{pair}"),
                    InlineKeyboardButton(f"Sell {symbol}", callback_data=f"sell_token_{pair}")
                ],
                [
                    InlineKeyboardButton("Positions", callback_data="__positions__"),
                    InlineKeyboardButton("Orders", callback_data="__orders__")
                ],
                [
                    InlineKeyboardButton("Settings", callback_data='settings')
                ]
            ] if default_exchange else [
                [
                    InlineKeyboardButton("Settings", callback_data='settings')
                ]
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            await updater.bot.send_message(chat_id=chat_id, parse_mode="HTML", text=f"Current <code>{symbol}</code> price is <code>{price}</code>{unit}.", reply_markup=reply_markup)
    return CEX_ROUTES

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Send message on `/start`."""
    # Get user that sent /start and log his name
    user = update.message.from_user
    chat_id = user.id
    if chat_id not in users:
        users.append(chat_id)
    logger.info("User %s started the conversation.", user.first_name)
    user_info = get_user_info_by_chat_id(update.message.from_user.id)
    if not user_info or not user_info.public_id:
        await update.message.reply_html(f'Please connect your chat id to our website. Your telegram chat id is <code>{chat_id}</code> 👉 <a href="https://app.artemisdigital.io/settings/trading">Connect</a>')
        return CEX_ROUTES
    default_exchange = get_default_exchange_by_user_id(user_info.public_id)
    if default_exchange:
        context.user_data["exchange"] = {
            "exchange_id": default_exchange.exchange,
            "api_key": default_exchange.api_key,
            "secret_key": default_exchange.secret_key,
            "password": default_exchange.api_passphrase,
            "account_name": default_exchange.account_name
        }
    # Build InlineKeyboard where each button has a displayed text
    # and a string as callback_data
    # The keyboard is a list of button rows, where each row is in turn
    # a list (hence `[[...]]`).
    if default_exchange:
        await update.message.reply_text(f"Your default exchange is {default_exchange.exchange.title()}({default_exchange.account_name}). You can handle your exchange API keys 👉 /exchanges")
    else :
        await update.message.reply_text("You haven't set a default exchange. Please set the default to buy or sell tokens 👉 /exchanges")
    return CEX_ROUTES

async def settings(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    query = update.callback_query
    await query.answer()
    if query.from_user.id in messages and "settings" in messages[query.from_user.id]:
        if messages[query.from_user.id]["settings"]:
            try :
                await update.effective_chat.delete_messages(message_ids=messages[query.from_user.id]["settings"])
            except :
                messages[query.from_user.id]["settings"] = None
    settings_reply_markup = InlineKeyboardMarkup([
        [InlineKeyboardButton('Menu', callback_data='menu'), InlineKeyboardButton('Close', callback_data='settings_menu_close')],
        [InlineKeyboardButton('API keys list', callback_data='api_keys')]
    ])
    # await query.edit_message_text(
    #     text="First CallbackQueryHandler, Choose a route", reply_markup=reply_markup
    # )
    # await query.edit_message_text(
    #     text="First CallbackQueryHandler, Choose a route", reply_markup=reply_markup
    # )
    setting_message = await update.effective_chat.send_message("═ ⚙️ Settings ═", reply_markup=settings_reply_markup)
    if query.from_user.id not in messages:
        messages[query.from_user.id] = {}
    messages[query.from_user.id]["settings"] = [setting_message.message_id]
    return CEX_ROUTES

async def settings_by_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    if update.message.from_user.id in messages and "settings" in messages[update.message.from_user.id]:
        if messages[update.message.from_user.id]["settings"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[update.message.from_user.id]["settings"])
            except :
                messages[update.message.from_user.id]["settings"] = None
    settings_reply_markup = InlineKeyboardMarkup([
        [InlineKeyboardButton('Menu', callback_data='menu'), InlineKeyboardButton('Close', callback_data='settings_menu_close')],
        [InlineKeyboardButton('API keys list', callback_data='api_keys')]
    ])
    setting_message = await update.effective_chat.send_message("═ ⚙️ Settings ═", reply_markup=settings_reply_markup)
    if update.message.from_user.id not in messages:
        messages[update.message.from_user.id] = {}
    messages[update.message.from_user.id]["settings"] = setting_message.message_id
    return CEX_ROUTES

async def settings_menu_close(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    query = update.callback_query
    await query.answer()
    await update.effective_chat.delete_message(query.message.message_id)
    return CEX_ROUTES

async def handle_cancel_connect_api_keys(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    query = update.callback_query
    await query.answer()
    await handle_api_keys(update, context)
    return CEX_ROUTES

async def handle_api_keys(update: Update, context: ContextTypes.DEFAULT_TYPE, origin=True) -> int:
    query = update.callback_query
    await query.answer()
    chat_id = update.callback_query.from_user.id
    api_keys = get_api_keys_by_chat_id(chat_id)
    user_info = get_user_info_by_chat_id(chat_id)
    default_exchange = get_default_exchange_by_user_id(user_info.public_id)
    connect_api_reply_markup = InlineKeyboardMarkup([
        [InlineKeyboardButton('Connect your CEX API keys', callback_data='connect_api_keys')],
        [InlineKeyboardButton('Cancel', callback_data='connect_api_keys_cancel')]
    ])
    if origin and update.callback_query.from_user.id in messages and "api_key_list" in messages[update.callback_query.from_user.id]:
        if messages[update.callback_query.from_user.id]["api_key_list"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[update.callback_query.from_user.id]["api_key_list"])
            except :
                messages[update.callback_query.from_user.id]["api_key_list"] = None
    if update.callback_query.from_user.id not in messages:
        messages[update.callback_query.from_user.id] = {}
    if api_keys:
        api_key_list_keyboard = [
            [
                InlineKeyboardButton(
                    f"✅ {api.exchange.replace('_', ' ').title()}({api.account_name})" if default_exchange and default_exchange.id == api.id else f"{api.exchange.replace('_', ' ').title()}({api.account_name})",
                    callback_data=f'handle_connected_api_keys_{api.id}'
                )
                for api in api_keys[key:key + 2]
            ]
            for key in range(0, len(api_keys), 2)
        ]
        api_key_list_keyboard.append([InlineKeyboardButton('Connect another API keys', callback_data='connect_api_keys')])
        api_key_list_keyboard.append([InlineKeyboardButton('Close', callback_data='api_keys_list_close')])
        api_key_list_markup = InlineKeyboardMarkup(api_key_list_keyboard)
        try :
            api_key_list_message = await update.callback_query.edit_message_text('List of API keys already connected', reply_markup=api_key_list_markup)
            messages[update.callback_query.from_user.id]["api_key_list"] = api_key_list_message.message_id
        except :
            """"""
    else :
        try :
            api_key_list_message = await update.callback_query.edit_message_text('There is no any connected API keys.', reply_markup=connect_api_reply_markup)
            messages[update.callback_query.from_user.id]["api_key_list"] = api_key_list_message.message_id
        except :
            """"""
        # return START_ROUTES
    return CEX_ROUTES

async def handle_api_keys_by_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat_id = update.message.from_user.id
    api_keys = get_api_keys_by_chat_id(chat_id)
    user_info = get_user_info_by_chat_id(chat_id)
    default_exchange = get_default_exchange_by_user_id(user_info.public_id)
    connect_api_reply_markup = InlineKeyboardMarkup([
        [InlineKeyboardButton('Connect your CEX API keys', callback_data='connect_api_keys')],
        [InlineKeyboardButton('Cancel', callback_data='connect_api_keys_cancel')],
    ])
    if update.message.from_user.id in messages and "api_key_list" in messages[update.message.from_user.id]:
        if messages[update.message.from_user.id]["api_key_list"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[update.message.from_user.id]["api_key_list"])
            except :
                messages[update.message.from_user.id]["api_key_list"] = None
    if api_keys:
        api_key_list_keyboard = [
            [
                InlineKeyboardButton(
                    f"✅ {api.exchange.replace('_', ' ').title()}({api.account_name})" if default_exchange and default_exchange.id == api.id else f"{api.exchange.replace('_', ' ').title()}({api.account_name})",
                    callback_data=f'handle_connected_api_keys_{api.id}'
                ) for api in api_keys[key:key + 2]
            ]
            for key in range(0, len(api_keys), 2)
        ]
        api_key_list_keyboard.append([InlineKeyboardButton('Connect another API keys', callback_data='connect_api_keys')])
        api_key_list_keyboard.append([InlineKeyboardButton('Close', callback_data='api_keys_list_close')])
        api_key_list_markup = InlineKeyboardMarkup(api_key_list_keyboard)
        api_key_list_message = await update.message.reply_text('List of API keys already connected', reply_markup=api_key_list_markup)
    else :
        api_key_list_message = await update.message.reply_text('There is no any connected API keys.', reply_markup=connect_api_reply_markup)
        # return START_ROUTES
    if update.message.from_user.id not in messages:
        messages[update.message.from_user.id] = {}
    messages[update.message.from_user.id]["api_key_list"] = api_key_list_message.message_id
    return CEX_ROUTES

async def handle_api_keys_list_close(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    query = update.callback_query
    try :
        await update.effective_chat.delete_message(query.message.message_id)
    finally :
        return CEX_ROUTES

async def handle_show_connect_api_keys(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    connect_cex_list_reply_keyboard = [
        [InlineKeyboardButton(cex["title"], callback_data=f'connect_{cex["slug"]}') for cex in cex_list1],
        [InlineKeyboardButton(cex["title"], callback_data=f'connect_{cex["slug"]}') for cex in cex_list2]
    ]
    connect_cex_list_reply_keyboard.append([
        InlineKeyboardButton("Cancel", callback_data="connect_api_keys_cancel")
    ])
    connect_cex_list_reply_markup = InlineKeyboardMarkup(connect_cex_list_reply_keyboard)
    await update.callback_query.edit_message_text('You can connect the API keys of the following CEX platforms.', reply_markup=connect_cex_list_reply_markup)
    return CEX_ROUTES

async def handle_connect_api_keys(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    exchange_id = update.callback_query.data.replace("connect_", "")
    for cex in cex_list1 + cex_list2:
        if cex["slug"] == exchange_id:
            try :
                account_message = await update.effective_chat.send_message(f"Input your {cex['title']} account name", reply_markup=ForceReply(selective=True))
                await update.effective_chat.delete_message(update.callback_query.message.message_id)
            except :
                """"""
            context.user_data['force_reply'] = {
                'message_id': account_message.message_id,
                'type': 'cex_account_name',
                'exchange_id': exchange_id
            }
            return CEX_ROUTES

async def handle_connected_api_keys(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    api_key_id = update.callback_query.data.replace('handle_connected_api_keys_', '')
    user_info = get_user_info_by_chat_id(update.callback_query.from_user.id)
    check_default = session.query(UserSettingsGlobal).filter_by(default_exchange=api_key_id, user_id=user_info.public_id).first()
    api_key = get_api_key_by_id(api_key_id)
    handle_connected_api_keys_markup = InlineKeyboardMarkup([
        [
            InlineKeyboardButton("Cancel as default" if check_default else "Set as default", callback_data=f"set_default_exchange_{api_key.id}"),
            InlineKeyboardButton("Disconnect", callback_data=f"disconnect_exchange_{api_key.id}")
        ],
        [
            InlineKeyboardButton("Close", callback_data="connected_api_keys_close")
        ]
    ])
    await update.callback_query.edit_message_text(f"Handle your {api_key.exchange.title()}({api_key.account_name}) account", reply_markup=handle_connected_api_keys_markup)

async def handle_set_default_exchange(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    api_key_id = update.callback_query.data.replace("set_default_exchange_", "")
    user_info = get_user_info_by_chat_id(update.callback_query.from_user.id)
    user_settings = session.query(UserSettingsGlobal).filter_by(user_id=user_info.public_id).first()
    if user_settings:
        if user_settings.default_exchange == api_key_id:
            user_settings.default_exchange = None
            context.user_data["exchange"] = {}
        else :
            api_key = get_api_key_by_id(api_key_id)
            user_settings.default_exchange = api_key_id
            context.user_data["exchange"] = {
                "exchange_id": api_key.exchange,
                "api_key": api_key.api_key,
                "secret_key": api_key.secret_key,
                "password": api_key.api_passphrase,
                "account_name": api_key.account_name
            }
        session.commit()
    await handle_api_keys(update, context, origin=False)
    return CEX_ROUTES

async def handle_remove_api_key(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    api_key_id = update.callback_query.data.replace("disconnect_exchange_", "")
    user_info = get_user_info_by_chat_id(update.callback_query.from_user.id)
    delete_api_key_by_id(api_key_id)
    default_api_key = get_default_exchange_by_user_id(user_info.public_id)
    if default_api_key and default_api_key.id == api_key_id:
        user_settings = session.query(UserSettingsGlobal).filter_by(user_id=user_info.public_id).first()
        user_settings.default_exchange = None
        context.user_data["exchange"] = {}
        session.commit()
    await handle_api_keys(update, context, origin=False)

async def handle_buy_token(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    ticker = update.callback_query.data.replace("buy_token_", "")
    symbol = ticker.split("/")[0]
    unit = ticker.split("/")[1]
    chat_id = update.callback_query.from_user.id
    exchange_data = context.user_data.get("exchange")
    if not exchange_data or not ticker:
        return CEX_ROUTES
    exchange = get_exchange_from_ccxt(exchange_data["exchange_id"], exchange_data["api_key"], exchange_data["secret_key"])
    if chat_id not in messages:
        messages[chat_id] = {}
    try :
        ticker_data = await exchange.fetch_ticker(ticker.replace("/", ""))
        buy_token_markup = InlineKeyboardMarkup([
            [
                InlineKeyboardButton("Limit", callback_data=f"limit_buy_{ticker}"),
                InlineKeyboardButton("Market", callback_data=f"market_buy_{ticker}")
            ]
        ])
        ticker = ticker.replace("/", "")
        price = ticker_data["info"]["lastPrice"]
        if "handle_token" in messages[chat_id] and messages[chat_id]["handle_token"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["handle_token"])
            except Exception as e:
                print("error --> ", e)
        buy_token_message = await update.effective_message.reply_html(
            f"═ Buy {ticker} ═\nCurrent <code>{ticker}</code> price is <code>{price}</code>{unit}.",
            reply_markup=buy_token_markup
        )
        messages[chat_id]["handle_token"] = buy_token_message.message_id
        if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
            finally :
                messages[chat_id]["select_position"] = None
                context.user_data["select_position"] = {}
        if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
            finally :
                messages[chat_id]["select_order"] = None
                context.user_data["select_order"] = None
    except Exception as e:
        print('error --> ', e)
    finally :
        await exchange.close()
    return CEX_ROUTES

async def handle_buy_limit_token(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    ticker = update.callback_query.data.replace("limit_buy_", "")
    symbol = ticker.split("/")[0]
    unit = ticker.split("/")[1]
    chat_id = update.callback_query.from_user.id
    if not ticker:
        return CEX_ROUTES
    if chat_id in messages:
        try :
            if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
                messages[chat_id]["select_position"] = None
            if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
                messages[chat_id]["select_order"] = None
            if "select_buy_token" in messages[chat_id] and messages[chat_id]["select_buy_token"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_buy_token"])
            if "select_sell_token" in messages[chat_id] and messages[chat_id]["select_sell_token"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_sell_token"])
        except :
            """"""
    limit_position_markup = InlineKeyboardMarkup([
        [
            InlineKeyboardButton(f"Price {unit}", callback_data="input_buy_price_for_limit"),
            InlineKeyboardButton(f"Qty {symbol}", callback_data="input_buy_qty_for_limit")
        ]
    ])
    select_buy_message = await update.effective_message.reply_html(
        f"═ Create Limit Buy Order (<code>{symbol}</code>) ═\n\nSend /buyclose to cancel.",
        reply_markup=limit_position_markup
    )
    context.user_data["require_limit_position_data"] = None
    context.user_data["require_market_position_data"] = None
    context.user_data["require_edit_order_data"] = None
    context.user_data["select_buy_token"] = {
        "symbol": symbol,
        "type": "limit",
        "unit": unit
    }
    if chat_id not in messages:
        messages[chat_id] = {}
    messages[chat_id]["select_buy_token"] = select_buy_message.message_id
    return CEX_ROUTES

async def handle_buy_market_token(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    ticker = update.callback_query.data.replace("market_buy_", "")
    symbol = ticker.split("/")[0]
    unit = ticker.split("/")[1]
    chat_id = update.callback_query.from_user.id
    if not ticker:
        return CEX_ROUTES
    if chat_id in messages:
        try :
            if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
                messages[chat_id]["select_position"] = None
            if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
                messages[chat_id]["select_order"] = None
            if "select_buy_token" in messages[chat_id] and messages[chat_id]["select_buy_token"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_buy_token"])
            if "select_sell_token" in messages[chat_id] and messages[chat_id]["select_sell_token"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_sell_token"])
        except :
            """"""
    limit_position_markup = InlineKeyboardMarkup([
        [
            InlineKeyboardButton(f"Qty {symbol}", callback_data="input_buy_qty_for_market")
        ]
    ])
    select_buy_message = await update.effective_message.reply_html(
        f"═ Create Market Buy Order (<code>{symbol}</code>) ═\n\nSend /buyclose to cancel.",
        reply_markup=limit_position_markup
    )
    context.user_data["require_limit_position_data"] = None
    context.user_data["require_market_position_data"] = None
    context.user_data["require_edit_order_data"] = None
    context.user_data["select_buy_token"] = {
        "symbol": symbol,
        "type": "market",
        "unit": unit
    }
    if chat_id not in messages:
        messages[chat_id] = {}
    messages[chat_id]["select_buy_token"] = select_buy_message.message_id
    return CEX_ROUTES

async def handle_buy_close(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat_id = update.message.from_user.id
    if chat_id in messages and "select_buy_token" in messages[chat_id] and messages[chat_id]["select_buy_token"]:
        try :
            await update.effective_chat.delete_message(message_id=messages[chat_id]["select_buy_token"])
            messages[chat_id]["select_buy_token"] = None
            await update.message.delete()
        except :
            """"""
    return CEX_ROUTES

async def handle_sell_limit_token(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    ticker = update.callback_query.data.replace("limit_sell_", "")
    symbol = ticker.split("/")[0]
    unit = ticker.split("/")[1]
    chat_id = update.callback_query.from_user.id
    if not ticker:
        return CEX_ROUTES
    if chat_id in messages:
        try :
            if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
                messages[chat_id]["select_position"] = None
            if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
                messages[chat_id]["select_order"] = None
            if "select_buy_token" in messages[chat_id] and messages[chat_id]["select_buy_token"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_buy_token"])
            if "select_sell_token" in messages[chat_id] and messages[chat_id]["select_sell_token"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_sell_token"])
        except :
            """"""
    limit_position_markup = InlineKeyboardMarkup([
        [
            InlineKeyboardButton(f"Price {unit}", callback_data="input_sell_price_for_limit"),
            InlineKeyboardButton(f"Qty {symbol}", callback_data="input_sell_qty_for_limit")
        ]
    ])
    select_sell_message = await update.effective_message.reply_html(
        f"═ Create Limit Sell Order (<code>{symbol}</code>) ═\n\nSend /sellclose to cancel.",
        reply_markup=limit_position_markup
    )
    context.user_data["require_limit_position_data"] = None
    context.user_data["require_market_position_data"] = None
    context.user_data["require_edit_order_data"] = None
    context.user_data["select_sell_token"] = {
        "symbol": symbol,
        "type": "limit",
        "unit": unit
    }
    if chat_id not in messages:
        messages[chat_id] = {}
    messages[chat_id]["select_sell_token"] = select_sell_message.message_id
    return CEX_ROUTES

async def handle_sell_market_token(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    ticker = update.callback_query.data.replace("market_sell_", "")
    symbol = ticker.split("/")[0]
    unit = ticker.split("/")[1]
    chat_id = update.callback_query.from_user.id
    if not ticker:
        return CEX_ROUTES
    if chat_id in messages:
        if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
            await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
            messages[chat_id]["select_position"] = None
        if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
            await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
            messages[chat_id]["select_order"] = None
        if "select_buy_token" in messages[chat_id] and messages[chat_id]["select_buy_token"]:
            await update.effective_chat.delete_message(message_id=messages[chat_id]["select_buy_token"])
        if "select_sell_token" in messages[chat_id] and messages[chat_id]["select_sell_token"]:
            await update.effective_chat.delete_message(message_id=messages[chat_id]["select_sell_token"])
    limit_position_markup = InlineKeyboardMarkup([
        [
            InlineKeyboardButton(f"Qty {symbol}", callback_data="input_sell_qty_for_market")
        ]
    ])
    select_sell_message = await update.effective_message.reply_html(
        f"═ Create Market Sell Order (<code>{symbol}</code>) ═\n\nSend /sellclose to cancel.",
        reply_markup=limit_position_markup
    )
    context.user_data["require_limit_position_data"] = None
    context.user_data["require_market_position_data"] = None
    context.user_data["require_edit_order_data"] = None
    context.user_data["select_sell_token"] = {
        "symbol": symbol,
        "type": "market",
        "unit": unit
    }
    if chat_id not in messages:
        messages[chat_id] = {}
    messages[chat_id]["select_sell_token"] = select_sell_message.message_id
    return CEX_ROUTES

async def handle_sell_close(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat_id = update.message.from_user.id
    if chat_id in messages and "select_sell_token" in messages[chat_id] and messages[chat_id]["select_sell_token"]:
        try :
            await update.effective_chat.delete_message(message_id=messages[chat_id]["select_sell_token"])
            messages[chat_id]["select_sell_token"] = None
            await update.message.delete()
        except :
            """"""
    return CEX_ROUTES

async def handle_click_price_for_buy_limit(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    select_buy_token = context.user_data.get("select_buy_token")
    if not select_buy_token:
        return CEX_ROUTES
    unit = select_buy_token["unit"]
    await update.effective_chat.send_message(f"Please enter the price in {unit}.")
    context.user_data["require_limit_buy_data"] = "price"
    return CEX_ROUTES

async def handle_click_qty_for_buy_limit(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    select_buy_token = context.user_data.get("select_buy_token")
    if not select_buy_token:
        return CEX_ROUTES
    symbol = select_buy_token["symbol"]
    await update.effective_chat.send_message(f"Please enter the qty in {symbol}.")
    context.user_data["require_limit_buy_data"] = "qty"
    return CEX_ROUTES

async def handle_click_qty_for_buy_market(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    select_buy_token = context.user_data.get("select_buy_token")
    if not select_buy_token:
        return CEX_ROUTES
    symbol = select_buy_token["symbol"]
    await update.effective_chat.send_message(f"Please enter the qty in {symbol}.")
    context.user_data["require_market_buy_data"] = "qty"
    return CEX_ROUTES

async def handle_click_price_for_sell_limit(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    select_sell_token = context.user_data.get("select_sell_token")
    if not select_sell_token:
        return CEX_ROUTES
    unit = select_sell_token["unit"]
    await update.effective_chat.send_message(f"Please enter the price in {unit}.")
    context.user_data["require_limit_sell_data"] = "price"
    return CEX_ROUTES

async def handle_click_qty_for_sell_limit(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    select_sell_token = context.user_data.get("select_sell_token")
    if not select_sell_token:
        return CEX_ROUTES
    symbol = select_sell_token["symbol"]
    await update.effective_chat.send_message(f"Please enter the qty in {symbol}.")
    context.user_data["require_limit_sell_data"] = "qty"
    return CEX_ROUTES

async def handle_click_qty_for_sell_market(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    select_sell_token = context.user_data.get("select_sell_token")
    if not select_sell_token:
        return CEX_ROUTES
    symbol = select_sell_token["symbol"]
    await update.effective_chat.send_message(f"Please enter the qty in {symbol}.")
    context.user_data["require_market_sell_data"] = "qty"
    return CEX_ROUTES

async def handle_sell_token(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    ticker = update.callback_query.data.replace("sell_token_", "")
    symbol = ticker.split("/")[0]
    unit = ticker.split("/")[1]
    chat_id = update.callback_query.from_user.id
    exchange_data = context.user_data.get("exchange")
    if not exchange_data:
        return CEX_ROUTES
    exchange = get_exchange_from_ccxt(exchange_data["exchange_id"], exchange_data["api_key"], exchange_data["secret_key"])
    if not chat_id in messages:
        messages[chat_id] = {}
    try :
        ticker_data = await exchange.fetch_ticker(ticker.replace("/", ""))
        price = ticker_data["info"]["lastPrice"]
        sell_token_markup = InlineKeyboardMarkup([
            [
                InlineKeyboardButton("Limit", callback_data=f"limit_sell_{ticker}"),
                InlineKeyboardButton("Market", callback_data=f"market_sell_{ticker}")
            ]
        ])
        ticker = ticker.replace("/", "")
        if "handle_token" in messages[chat_id] and messages[chat_id]["handle_token"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["handle_token"])
            except Exception as e:
                print("error --> ", e)
        sell_token_message = await update.effective_message.reply_html(
            f"═ Sell {ticker.replace('/', '')} ═\nCurrent <code>{ticker}</code> price is <code>{price}</code>{unit}.",
            reply_markup=sell_token_markup
        )
        messages[chat_id]["handle_token"] = sell_token_message.message_id
        if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
            finally :
                messages[chat_id]["select_position"] = None
                context.user_data["select_position"] = {}
        if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
            finally :
                messages[chat_id]["select_order"] = None
                context.user_data["select_order"] = None
    except Exception as e:
        print("error --> ", e)
    finally :
        await exchange.close()
    return CEX_ROUTES

def get_exchange_from_ccxt(exchange_id: str, api_key: str, secret_key: str, password = "") -> Exchange:
    if exchange_id == "bybit":
        exchange = ccxt.bybit({
            "apiKey": api_key,
            "secret": secret_key,
            "options": {
                "recvWindow": 10000,
                "adjustForTimeDifference": True
            }
        })
    elif exchange_id == "kucoinfutures":
        exchange = ccxt.kucoinfutures({
            "apiKey": api_key,
            "secret": secret_key,
            "options": {
                "recvWindow": 10000,
                "adjustForTimeDifference": True
            }
        })
    elif exchange_id == "binance":
        exchange = ccxt.binance({
            "apiKey": api_key,
            "secret": secret_key,
            "options": {
                "recvWindow": 10000,
                "adjustForTimeDifference": True
            }
        })
    elif exchange_id == "bitget":
        exchange = ccxt.bitget({
            "apiKey": api_key,
            "secret": secret_key,
            "password": password,
            "options": {
                "recvWindow": 10000,
                "adjustForTimeDifference": True
            }
        })
    elif exchange_id == "okx":
        exchange = ccxt.okx({
            "apiKey": api_key,
            "secret": secret_key,
            "password": password,
            "options": {
                "recvWindow": 10000,
                "adjustForTimeDifference": True
            }
        })
    elif exchange_id == "gate" or exchange_id == "gateio":
        exchange = ccxt.gate({
            "apiKey": api_key,
            "secret": secret_key,
            "options": {
                "recvWindow": 10000,
                "adjustForTimeDifference": True
            }
        })
    elif exchange_id == "woo":
        exchange = ccxt.woo({
            "apiKey": api_key,
            "secret": secret_key,
            "options": {
                "recvWindow": 10000,
                "adjustForTimeDifference": True
            }
        })
    return exchange

async def handle_positions(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat_id = update.callback_query.from_user.id
    user_info = get_user_info_by_chat_id(chat_id)
    default_exchange = get_default_exchange_by_user_id(user_info.public_id)
    if chat_id in messages:
        if "position_list" in messages[chat_id] and len(messages[chat_id]["position_list"]):
            try :
                await update.effective_chat.delete_messages(message_ids=messages[chat_id]["position_list"])
            except :
                """"""
        if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
            finally :
                messages[chat_id]["select_position"] = None
        if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
            finally :
                messages[chat_id]["select_order"] = None
                context.user_data["select_order"] = None
    if default_exchange:
        position_lists = []
        try :
            exchange = get_exchange_from_ccxt(default_exchange.exchange, api_key=default_exchange.api_key, secret_key=default_exchange.secret_key)
            positions = await exchange.fetch_positions()
            context.user_data["exchange"] = {
                "exchange_id": default_exchange.exchange,
                "api_key": default_exchange.api_key,
                "secret_key": default_exchange.secret_key,
                "password": default_exchange.api_passphrase,
                "account_name": default_exchange.account_name
            }
            context.user_data["select_position"] = None
            context.user_data["positions"] = {}
            if len(positions):
                for position in positions:
                    unit = position["symbol"].split(":")[1]
                    symbol = position["info"]["symbol"]
                    handle_position_markup = InlineKeyboardMarkup([
                        [
                            InlineKeyboardButton("Limit", callback_data=f"position_limit_{symbol}"),
                            InlineKeyboardButton("Market", callback_data=f"position_market_{symbol}")
                        ]
                    ])
                    position_list = await update.effective_message.reply_html(
                        f"═ {position['side'].title()} Position ═ \n Symbol: <code>{symbol}</code> \n Value: <code>{position['info']['positionValue']}</code>{unit} \n Qty: <code>{position['info']['size']}</code> \n Entry Price: <code>{position['entryPrice']}</code> \n LiqPrice: <code>{position['info']['liqPrice']}</code> \n Unrealized PNL: <code>{position['unrealizedPnl']}</code> \n Realized PNL: <code>{position['realizedPnl']}</code>",
                        reply_markup=handle_position_markup
                    )
                    position_lists.append(position_list.message_id)
                    context.user_data["positions"][symbol] = {
                        "symbol": symbol,
                        "qty": position["info"]["size"],
                        "price": position["entryPrice"],
                        "unit": unit,
                        "ticker": position["symbol"].split("/")[0],
                        "side": position["side"]
                    }
            else :
                position_list = await update.effective_message.reply_html(
                    f"═ Positions | {default_exchange.exchange.title()} ═ \n No active positions"
                )
                position_lists.append(position_list.message_id)
        except Exception as error:
            print('error->', error)
            await update.effective_message.reply_text(f"Your {default_exchange.exchange.title()} API key is invalid. \nYou can handle your API keys 👉 /exchanges")
        finally :
            if chat_id not in messages:
                messages[chat_id] = {}
            messages[chat_id]["position_list"] = position_lists
            await exchange.close()
    else :
        await update.effective_chat.send_message("You haven't set a default exchange. Please set the default to buy or sell tokens 👉 /exchanges")
    return CEX_ROUTES

async def handle_positions_by_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat_id = update.message.from_user.id
    user_info = get_user_info_by_chat_id(chat_id)
    default_exchange = get_default_exchange_by_user_id(user_info.public_id)
    if chat_id in messages:
        if "position_list" in messages[chat_id] and len(messages[chat_id]["position_list"]):
            try :
                await update.effective_chat.delete_messages(message_ids=messages[chat_id]["position_list"])
            except :
                """"""
        if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
            except :
                """"""
            messages[chat_id]["select_position"] = None
        if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
            except :
                """"""
            context.user_data["select_order"] = None
            messages[chat_id]["select_order"] = None
    if default_exchange:
        position_lists = []
        try :
            exchange = get_exchange_from_ccxt(default_exchange.exchange, api_key=default_exchange.api_key, secret_key=default_exchange.secret_key)
            positions = await exchange.fetch_positions()
            context.user_data["exchange"] = {
                "exchange_id": default_exchange.exchange,
                "api_key": default_exchange.api_key,
                "secret_key": default_exchange.secret_key,
                "password": default_exchange.api_passphrase,
                "account_name": default_exchange.account_name
            }
            context.user_data["select_position"] = None
            context.user_data["positions"] = {}
            if len(positions):
                for position in positions:
                    unit = position["symbol"].split(":")[1]
                    symbol = position["info"]["symbol"]
                    handle_position_markup = InlineKeyboardMarkup([
                        [
                            InlineKeyboardButton("Limit", callback_data=f"position_limit_{symbol}"),
                            InlineKeyboardButton("Market", callback_data=f"position_market_{symbol}")
                        ]
                    ])
                    position_list = await update.message.reply_html(
                        f"═ {position['side'].title()} Position ═ \n Symbol: <code>{symbol}</code> \n Value: <code>{position['info']['positionValue']}</code>USDT \n Qty: <code>{position['info']['size']}</code> \n Entry Price: <code>{position['entryPrice']}</code> \n LiqPrice: <code>{position['info']['liqPrice']}</code> \n Unrealized PNL: <code>{position['unrealizedPnl']}</code> \n Realized PNL: <code>{position['realizedPnl']}</code>",
                        reply_markup=handle_position_markup
                    )
                    position_lists.append(position_list.message_id)
                    context.user_data["positions"][symbol] = {
                        "symbol": symbol,
                        "qty": position["info"]["size"],
                        "price": position["entryPrice"],
                        "unit": unit,
                        "ticker": position["symbol"].split("/")[0],
                        "side": position["side"]
                    }
            else :
                position_list = await update.message.reply_html(
                    f"═ Positions | {default_exchange.exchange.title()} ═ \n No active positions"
                )
                position_lists.append(position_list.message_id)
        except Exception as error:
            print('error->', error)
            await update.message.reply_text(f"Your {default_exchange.exchange.title()} API key is invalid. \nYou can handle your API keys 👉 /exchanges")
        finally :
            if chat_id not in messages:
                messages[chat_id] = {}
            messages[chat_id]["position_list"] = position_lists
            await exchange.close()
    else :
        await update.message.reply_text("You haven't set a default exchange. Please set the default to buy or sell tokens 👉 /exchanges")
    return CEX_ROUTES

async def handle_orders(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat_id = update.callback_query.from_user.id
    user_info = get_user_info_by_chat_id(chat_id)
    default_exchange = get_default_exchange_by_user_id(user_info.public_id)
    if chat_id in messages:
        if "order_list" in messages[chat_id] and len(messages[chat_id]["order_list"]):
            try :
                await update.effective_chat.delete_messages(message_ids=messages[chat_id]["order_list"])
            except :
                """"""
        if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
            except :
                """"""
            finally :
                messages[chat_id]["select_order"] = None
        if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
            except :
                """"""
            finally :
                messages[chat_id]["select_position"] = None
                context.user_data["select_position"] = None
    if default_exchange:
        order_lists = []
        try :
            exchange = get_exchange_from_ccxt(default_exchange.exchange, api_key=default_exchange.api_key, secret_key=default_exchange.secret_key)
            orders = await exchange.fetch_open_orders()
            context.user_data["select_order"] = None
            context.user_data["exchange"] = {
                "exchange_id": default_exchange.exchange,
                "api_key": default_exchange.api_key,
                "secret_key": default_exchange.secret_key,
                "password": default_exchange.api_passphrase,
                "account_name": default_exchange.account_name
            }
            context.user_data["orders"] = {}
            if len(orders):
                for order in orders:
                    pair = order["symbol"]
                    symbol = pair.split("/")[0]
                    id = order["id"]
                    handle_order_markup = InlineKeyboardMarkup([
                        [
                            InlineKeyboardButton("Edit", callback_data=f"order_edit_{id}"),
                            InlineKeyboardButton("Cancel", callback_data=f"order_cancel_{id}")
                        ]
                    ])
                    order_list = await update.effective_message.reply_html(
                        f"═ {order['info']['orderType']} {order['info']['side']} Order ═ \n Symbol: <code>{order['info']['symbol']}</code> \n Price: <code>{order['info']['price']}</code> \n Amount: <code>{order['amount']}</code>{symbol}",
                        reply_markup=handle_order_markup
                    )
                    context.user_data["orders"][order["id"]] = {
                        "price": order["info"]["price"],
                        "type": order["info"]["orderType"],
                        "amount": order["amount"],
                        "symbol": order["info"]["symbol"],
                        "side": order["info"]["side"],
                        "unit": pair.split(":")[1],
                        "ticker": pair.split("/")[0]
                    }
                    order_lists.append(order_list.message_id)
            else :
                order_list = await update.effective_message.reply_html(
                    f"═ Orders | {default_exchange.exchange.title()} ═ \n No active orders"
                )
                order_lists.append(order_list.message_id)
        except Exception as error:
            print('error->', error)
            await update.effective_message.reply_text(f"Your {default_exchange.exchange.title()} API key is invalid. \nYou can handle your API keys 👉 /exchanges")
        finally :
            if chat_id not in messages:
                messages[chat_id] = {}
            messages[chat_id]["order_list"] = order_lists
            await exchange.close()
    else :
        await update.effective_chat.send_message("You haven't set a default exchange. Please set the default to buy or sell tokens 👉 /exchanges")
    return CEX_ROUTES

async def handle_orders_by_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat_id = update.message.from_user.id
    user_info = get_user_info_by_chat_id(chat_id)
    default_exchange = get_default_exchange_by_user_id(user_info.public_id)
    if chat_id in messages:
        if "order_list" in messages[chat_id] and len(messages[chat_id]["order_list"]):
            try :
                await update.effective_chat.delete_messages(message_ids=messages[chat_id]["order_list"])
            except :
                """"""
        if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
            finally :
                messages[chat_id]["select_order"] = None
        if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
            finally :
                messages[chat_id]["select_position"] = None
                context.user_data["select_position"] = None
    if default_exchange:
        order_lists = []
        try :
            exchange = get_exchange_from_ccxt(default_exchange.exchange, api_key=default_exchange.api_key, secret_key=default_exchange.secret_key)
            orders = await exchange.fetch_open_orders()
            context.user_data["select_order"] = None
            context.user_data["orders"] = {}
            context.user_data["exchange"] = {
                "exchange_id": default_exchange.exchange,
                "api_key": default_exchange.api_key,
                "secret_key": default_exchange.secret_key,
                "password": default_exchange.api_passphrase,
                "account_name": default_exchange.account_name
            }
            if len(orders):
                for order in orders:
                    pair = order["symbol"]
                    symbol = pair.split("/")[0]
                    id = order["id"]
                    handle_order_markup = InlineKeyboardMarkup([
                        [
                            InlineKeyboardButton("Edit", callback_data=f"order_edit_{id}"),
                            InlineKeyboardButton("Cancel", callback_data=f"order_cancel_{id}")
                        ]
                    ])
                    order_list = await update.message.reply_html(
                        f"═ {order['info']['orderType']} {order['info']['side']} Order ═ \n Symbol: <code>{order['info']['symbol']}</code> \n Price: <code>{order['info']['price']}</code> \n Amount: <code>{order['amount']}</code>{symbol}",
                        reply_markup=handle_order_markup
                    )
                    context.user_data["orders"][order["id"]] = {
                        "price": order["info"]["price"],
                        "type": order["info"]["orderType"],
                        "amount": order["amount"],
                        "symbol": order["info"]["symbol"],
                        "side": order["info"]["side"],
                        "unit": pair.split(":")[1],
                        "ticker": pair.split("/")[0]
                    }
                    order_lists.append(order_list.message_id)
            else :
                order_list = await update.message.reply_html(
                    f"═ Orders | {default_exchange.exchange.title()} ═ \n No active orders"
                )
                order_lists.append(order_list.message_id)
        except Exception as error:
            print('error->', error)
            await update.message.reply_text(f"Your {default_exchange.exchange.title()} API key is invalid. \nYou can handle your API keys 👉 /exchanges")
        finally :
            if chat_id not in messages:
                messages[chat_id] = {}
            messages[chat_id]["order_list"] = order_lists
            await exchange.close()
    else :
        await update.message.reply_text("You haven't set a default exchange. Please set the default to buy or sell tokens 👉 /exchanges")
    return CEX_ROUTES

async def handle_cancel_order(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    order_id = update.callback_query.data.replace("order_cancel_", "")
    orders = context.user_data.get("orders")
    exchange_data = context.user_data.get("exchange")
    if not exchange_data or not orders:
        return CEX_ROUTES
    exchange = get_exchange_from_ccxt(exchange_data["exchange_id"], exchange_data["api_key"], exchange_data["secret_key"])
    try :
        order = await exchange.fetch_open_order(order_id)
        symbol = order["info"]["symbol"]
        await exchange.cancel_order(order_id, symbol=symbol)
        await update.callback_query.delete_message()
        if update.callback_query.from_user.id in messages and "order_list" in messages[update.callback_query.from_user.id]:
            messages[update.callback_query.from_user.id]["order_list"].remove(update.callback_query.message.message_id)
    except Exception as e:
        print("error --> ", e)
    finally :
        await exchange.close()
    return CEX_ROUTES

async def handle_edit_order(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat_id = update.callback_query.from_user.id
    order_id = update.callback_query.data.replace("order_edit_", "")
    orders = context.user_data.get("orders")
    exchange_data = context.user_data.get("exchange")
    if not exchange_data or not orders:
        return CEX_ROUTES
    exchange = get_exchange_from_ccxt(exchange_data["exchange_id"], exchange_data["api_key"], exchange_data["secret_key"])
    try :
        if chat_id in messages:
            if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
                messages[chat_id]["select_order"] = None
            if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
                messages[chat_id]["select_position"] = None
        if orders and order_id in orders:
            order = orders[order_id]
            edit_order_markup = InlineKeyboardMarkup([
                [
                    InlineKeyboardButton(f"Price {order['unit']}", callback_data="input_order_price_for_edit"),
                    InlineKeyboardButton(f"Amount {order['ticker']}", callback_data="input_order_amount_for_edit")
                ]
            ])
            select_order_message = await update.effective_message.reply_html(
                f"═ Edit {order['type']} {order['side']} Order (<code>{order['symbol']}</code>) ═ \nPrice: <code>{order['price']}</code> \nAmount: <code>{order['amount']}</code> \n\nSend /orderclose to keep the current order. \nSend /ordercancel to cancel the current order.",
                reply_markup=edit_order_markup
            )
            context.user_data["require_limit_position_data"] = None
            context.user_data["require_market_position_data"] = None
            context.user_data["require_edit_order_data"] = None
        if update.callback_query.from_user.id not in messages:
            messages[update.callback_query.from_user.id] = {}
        messages[update.callback_query.from_user.id]["select_order"] = select_order_message.message_id
        context.user_data["select_order"] = order_id
    except Exception as e:
        print("error --> ", e)
    finally :
        await exchange.close()
    return CEX_ROUTES

async def handle_order_close(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    if update.message.from_user.id in messages and "select_order" in messages[update.message.from_user.id]:
        if messages[update.message.from_user.id]["select_order"]:
            try:
                await update.effective_chat.delete_message(message_id=messages[update.message.from_user.id]["select_order"])
            finally :
                messages[update.message.from_user.id]["select_order"] = None
                context.user_data["select_order"] = None
                context.user_data["require_limit_position_data"] = None
                context.user_data["require_market_position_data"] = None
                context.user_data["require_edit_order_data"] = None
    await update.message.delete()
    return CEX_ROUTES

async def handle_order_cancel(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    context.user_data["require_limit_position_data"] = None
    context.user_data["require_market_position_data"] = None
    context.user_data["require_edit_order_data"] = None
    return CEX_ROUTES

async def handle_position_close(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat_id = update.message.from_user.id
    if chat_id in messages and "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
        try :
            await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
        finally :
            messages[chat_id]["select_position"] = None
            context.user_data["select_position"] = None
            context.user_data["require_limit_position_data"] = None
            context.user_data["require_market_position_data"] = None
            context.user_data["require_edit_order_data"] = None
    await update.message.delete()
    return CEX_ROUTES

async def reply_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    force_reply_data = context.user_data.get('force_reply')
    if force_reply_data and update.message.reply_to_message:
        if update.message.reply_to_message.message_id == force_reply_data['message_id']:
            if force_reply_data['type'] == 'cex_account_name':
                account_name = update.message.text
                exchange_id = force_reply_data['exchange_id']
                try :
                    api_key_message = await update.effective_chat.send_message(f"Input your {exchange_id.title()} account API key", reply_markup=ForceReply(selective=True))
                    await update.effective_chat.delete_message(update.message.reply_to_message.message_id)
                except :
                    """"""
                context.user_data['force_reply'] = {
                    'message_id': api_key_message.message_id,
                    'type': 'cex_account_api_key',
                    'exchange_id': exchange_id,
                    'account_name': account_name
                }
            if force_reply_data['type'] == 'cex_account_api_key':
                api_key = update.message.text
                exchange_id = force_reply_data['exchange_id']
                account_name = force_reply_data['account_name']
                try :
                    secret_key_message = await update.effective_chat.send_message(f"Input your {exchange_id.title()} account secret key", reply_markup=ForceReply(selective=True))
                    await update.effective_chat.delete_message(update.message.reply_to_message.message_id)
                except :
                    """"""
                context.user_data['force_reply'] = {
                    'message_id': secret_key_message.message_id,
                    'type': 'cex_account_secret_key',
                    'exchange_id': exchange_id,
                    'account_name': account_name,
                    'api_key': api_key
                }
            if force_reply_data['type'] == 'cex_account_secret_key':
                secret_key = update.message.text
                exchange_id = force_reply_data['exchange_id']
                account_name = force_reply_data['account_name']
                api_key = force_reply_data['api_key']
                exchange = get_exchange_from_ccxt(exchange_id=exchange_id, api_key=api_key, secret_key=secret_key)
                try :
                    await exchange.load_markets()
                    success_message = await update.message.reply_html(f"Your {exchange_id.title()} (<code>{account_name}</code>) account is successfully connected!")
                    user_info = get_user_info_by_chat_id(update.message.from_user.id)
                    new_api_key = ApiKey(
                        user_id=user_info.public_id,
                        api_key=api_key,
                        secret_key=secret_key,
                        account_name=account_name,
                        api_passphrase="",
                        exchange=exchange_id
                    )
                    new_api_key.insert()
                    session.commit()
                    await exchange.close()
                    await handle_api_keys_by_command(update, context)
                    await success_message.delete()
                except Exception as e:
                    print('error ===> ', e)
                    await update.message.reply_text("Your API key or secret key is invalid.")
                finally :
                    await update.effective_chat.delete_message(update.message.reply_to_message.message_id)
    return CEX_ROUTES

async def chat_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    positions = context.user_data.get("positions")
    select_position = context.user_data.get("select_position")
    orders = context.user_data.get("orders")
    order_id = context.user_data.get("select_order")
    select_buy_token = context.user_data.get("select_buy_token")
    select_sell_token = context.user_data.get("select_sell_token")
    if select_buy_token:
        if select_buy_token["type"] == "limit":
            require_limit_buy_data = context.user_data.get("require_limit_buy_data")
            if require_limit_buy_data == "price":
                price = update.message.text
                if not is_float_string(price):
                    await update.message.reply_text("Invalid price. Please enter number only!")
                    return CEX_ROUTES
                if not context.user_data.get("select_buy_token_data"):
                    context.user_data["select_buy_token_data"] = {}
                context.user_data["select_buy_token_data"]["symbol"] = f"{select_buy_token['symbol']}{select_buy_token['unit']}"
                context.user_data["select_buy_token_data"]["price"] = price
            if require_limit_buy_data == "qty":
                qty = update.message.text
                if not is_float_string(qty):
                    await update.message.reply_text("Invalid qty. Please enter number only!")
                    return CEX_ROUTES
                if not context.user_data.get("select_buy_token_data"):
                    context.user_data["select_buy_token_data"] = {}
                context.user_data["select_buy_token_data"]["symbol"] = f"{select_buy_token['symbol']}{select_buy_token['unit']}"
                context.user_data["select_buy_token_data"]["qty"] = qty
            select_buy_token_data = context.user_data.get("select_buy_token_data")
            if select_buy_token_data and "symbol" in select_buy_token_data and "qty" in select_buy_token_data and "price" in select_buy_token_data:
                symbol = select_buy_token_data["symbol"]
                price = select_buy_token_data["price"]
                qty = select_buy_token_data["qty"]
                exchange_data = context.user_data.get("exchange")
                if exchange_data:
                    exchange = get_exchange_from_ccxt(exchange_data["exchange_id"], exchange_data["api_key"], exchange_data["secret_key"])
                    try :
                        await exchange.create_limit_buy_order(symbol=symbol, price=price, amount=qty)
                        await update.message.reply_text("Your order was successfully created!")
                        await handle_orders_by_command(update, context)
                    except Exception as e:
                        print("error --> ", e)
                        await update.message.reply_text("Something went wrong!")
                    finally :
                        context.user_data["select_buy_token_data"] = None
                        context.user_data["select_buy_token"] = None
                        await exchange.close()
        if select_buy_token["type"] == "market":
            require_market_buy_data = context.user_data.get("require_market_buy_data")
            if require_market_buy_data == "qty":
                qty = update.message.text
                if not is_float_string(qty):
                    await update.message.reply_text("Invalid qty. Please enter number only!")
                    return CEX_ROUTES
                if not context.user_data.get("select_buy_token_data"):
                    context.user_data["select_buy_token_data"] = {}
                context.user_data["select_buy_token_data"]["symbol"] = f"{select_buy_token['symbol']}{select_buy_token['unit']}"
                context.user_data["select_buy_token_data"]["qty"] = qty
            select_buy_token_data = context.user_data.get("select_buy_token_data")
            if select_buy_token_data and "symbol" in select_buy_token_data and "qty" in select_buy_token_data:
                symbol = select_buy_token_data["symbol"]
                qty = select_buy_token_data["qty"]
                exchange_data = context.user_data.get("exchange")
                if exchange_data:
                    exchange = get_exchange_from_ccxt(exchange_data["exchange_id"], exchange_data["api_key"], exchange_data["secret_key"])
                    try :
                        await exchange.create_market_buy_order(symbol=symbol, amount=qty)
                        await update.message.reply_text("Your order was successfully created!")
                        await handle_positions_by_command(update, context)
                    except Exception as e:
                        print("error --> ", e)
                        await update.message.reply_text("Something went wrong!")
                    finally :
                        context.user_data["select_buy_token_data"] = None
                        context.user_data["select_buy_token"] = None
                        await exchange.close()
    if select_sell_token:
        if select_sell_token["type"] == "limit":
            require_limit_sell_data = context.user_data.get("require_limit_sell_data")
            if require_limit_sell_data == "price":
                price = update.message.text
                if not is_float_string(price):
                    await update.message.reply_text("Invalid price. Please enter number only!")
                    return CEX_ROUTES
                if not context.user_data.get("select_sell_token_data"):
                    context.user_data["select_sell_token_data"] = {}
                context.user_data["select_sell_token_data"]["symbol"] = f"{select_sell_token['symbol']}{select_sell_token['unit']}"
                context.user_data["select_sell_token_data"]["price"] = price
            if require_limit_sell_data == "qty":
                qty = update.message.text
                if not is_float_string(qty):
                    await update.message.reply_text("Invalid qty. Please enter number only!")
                    return CEX_ROUTES
                if not context.user_data.get("select_sell_token_data"):
                    context.user_data["select_sell_token_data"] = {}
                context.user_data["select_sell_token_data"]["symbol"] = f"{select_sell_token['symbol']}{select_sell_token['unit']}"
                context.user_data["select_sell_token_data"]["qty"] = qty
            select_sell_token_data = context.user_data.get("select_sell_token_data")
            if select_sell_token_data and "symbol" in select_sell_token_data and "qty" in select_sell_token_data and "price" in select_sell_token_data:
                symbol = select_sell_token_data["symbol"]
                price = select_sell_token_data["price"]
                qty = select_sell_token_data["qty"]
                exchange_data = context.user_data.get("exchange")
                if exchange_data:
                    exchange = get_exchange_from_ccxt(exchange_data["exchange_id"], exchange_data["api_key"], exchange_data["secret_key"])
                    try :
                        await exchange.create_limit_sell_order(symbol=symbol, price=price, amount=qty)
                        await update.message.reply_text("Your order was successfully created!")
                        await handle_orders_by_command(update, context)
                    except Exception as e:
                        print("error --> ", e)
                        await update.message.reply_text("Something went wrong!")
                    finally :
                        context.user_data["select_sell_token_data"] = None
                        context.user_data["select_sell_token"] = None
                        await exchange.close()
        if select_sell_token["type"] == "market":
            require_market_sell_data = context.user_data.get("require_market_sell_data")
            if require_market_sell_data == "qty":
                qty = update.message.text
                if not is_float_string(qty):
                    await update.message.reply_text("Invalid qty. Please enter number only!")
                    return CEX_ROUTES
                if not context.user_data.get("select_sell_token_data"):
                    context.user_data["select_sell_token_data"] = {}
                context.user_data["select_sell_token_data"]["symbol"] = f"{select_sell_token['symbol']}{select_sell_token['unit']}"
                context.user_data["select_sell_token_data"]["qty"] = qty
            select_sell_token_data = context.user_data.get("select_sell_token_data")
            if select_sell_token_data and "symbol" in select_sell_token_data and "qty" in select_sell_token_data:
                symbol = select_sell_token_data["symbol"]
                qty = select_sell_token_data["qty"]
                exchange_data = context.user_data.get("exchange")
                if exchange_data:
                    exchange = get_exchange_from_ccxt(exchange_data["exchange_id"], exchange_data["api_key"], exchange_data["secret_key"])
                    try :
                        await exchange.create_market_buy_order(symbol=symbol, amount=qty)
                        await update.message.reply_text("Your order was successfully created!")
                        await handle_positions_by_command(update, context)
                    except Exception as e:
                        print("error --> ", e)
                        await update.message.reply_text("Something went wrong!")
                    finally :
                        context.user_data["select_sell_token_data"] = None
                        context.user_data["select_sell_token"] = None
                        await exchange.close()
    if orders and order_id in orders:
        order = orders[order_id]
        require_edit_order_data = context.user_data.get("require_edit_order_data")
        text = update.message.text
        if require_edit_order_data == "price":
            if not is_float_string(text):
                await update.message.reply_text("Invalid price. Please enter number only!")
                return CEX_ROUTES
            if not context.user_data.get("edit_order_data"):
                context.user_data["edit_order_data"] = {}
            context.user_data["edit_order_data"]["price"] = text
            if "amount" not in context.user_data.get("edit_order_data"):
                await update.effective_chat.send_message(f"Please enter the amount in {order['ticker']}.")
                context.user_data["require_edit_order_data"] = "amount"
        if require_edit_order_data == "amount":
            if not is_float_string(text):
                await update.message.reply_text("Invalid amount. Please enter number only!")
                return CEX_ROUTES
            if not context.user_data.get("edit_order_data"):
                context.user_data["edit_order_data"] = {}
            context.user_data["edit_order_data"]["amount"] = text
            if "price" not in context.user_data.get("edit_order_data"):
                try :
                    await update.effective_chat.send_message(f"Please enter the price in {order['unit']}.")
                finally :
                    context.user_data["require_edit_order_data"] = "price"
        edit_order_data = context.user_data.get("edit_order_data")
        if edit_order_data and "price" in edit_order_data and "amount" in edit_order_data:
            price = edit_order_data["price"]
            amount = edit_order_data["amount"]
            if not price or not amount:
                return CEX_ROUTES
            try:
                exchange_data = context.user_data.get("exchange")
                if exchange_data:
                    exchange = get_exchange_from_ccxt(exchange_data["exchange_id"], exchange_data["api_key"], exchange_data["secret_key"])
                    await exchange.edit_order(
                        order_id,
                        symbol=order["symbol"],
                        type=order["type"],
                        side=order["side"],
                        price=price,
                        amount=amount
                    )
                    context.user_data["require_edit_order_data"] = None
                    context.user_data["edit_order_data"] = {}
                    await update.message.reply_text("Your order has been successfully updated!")
                    await handle_orders_by_command(update, context)
            except Exception as e:
                print("error --> ", e)
                await update.message.reply_text("Something went wrong. Please try again, or you can send /orderclose to keep the current order.")
            finally :
                await exchange.close()
    if positions and select_position and "symbol" in select_position:
        symbol = select_position["symbol"]
        if symbol in positions and positions[symbol]:
            position = positions[symbol]
            require_limit_position_data = context.user_data.get("require_limit_position_data")
            require_market_position_data = context.user_data.get("require_market_position_data")
            text = update.message.text
            if require_limit_position_data == "price":
                if not is_float_string(text):
                    await update.message.reply_text("Invalid price. Please enter number only!")
                    return CEX_ROUTES
                if not context.user_data.get("limit_position_data"):
                    context.user_data["limit_position_data"] = {}
                context.user_data["limit_position_data"]["price"] = text
                context.user_data["market_position_data"] = {}
                context.user_data["require_market_position_data"] = None
                if "qty" not in context.user_data.get("limit_position_data"):
                    await update.effective_chat.send_message(f"Please enter the qty in {position['ticker']}.")
                    context.user_data["require_limit_position_data"] = "qty"
            if require_limit_position_data == "qty":
                if not is_float_string(text):
                    await update.message.reply_text("Invalid qty. Please enter number only!")
                    return CEX_ROUTES
                if not context.user_data.get("limit_position_data"):
                    context.user_data["limit_position_data"] = {}
                context.user_data["limit_position_data"]["qty"] = text
                context.user_data["market_position_data"] = {}
                context.user_data["require_market_position_data"] = None
                if "price" not in context.user_data.get("limit_position_data"):
                    await update.effective_chat.send_message(f"Please enter the price in {position['unit']}.")
                    context.user_data["require_limit_position_data"] = "price"
            if require_market_position_data == "qty":
                if not is_float_string(text):
                    await update.message.reply_text("Invalid qty. Please enter number only!")
                    return CEX_ROUTES
                if not context.user_data.get("market_position_data"):
                    context.user_data["market_position_data"] = {}
                context.user_data["market_position_data"]["qty"] = text
                context.user_data["require_limit_position_data"] = None
                context.user_data["limit_position_data"] = {}
            if context.user_data.get("market_position_data") and "qty" in context.user_data.get("market_position_data"):
                exchange_data = context.user_data.get("exchange")
                market_position_data = context.user_data.get("market_position_data")
                if exchange_data and market_position_data["qty"]:
                    exchange = get_exchange_from_ccxt(exchange_data["exchange_id"], exchange_data["api_key"], exchange_data["secret_key"])
                    try :
                        await exchange.create_market_sell_order(symbol=symbol, amount=market_position_data["qty"])
                        await update.effective_chat.send_message("Market order was successfully created!")
                        await handle_positions_by_command(update, context)
                        await handle_orders_by_command(update, context)
                    except Exception as e:
                        print("error --> ", e)
                    finally :
                        await exchange.close()
                context.user_data["market_position_data"] = {}
                context.user_data["limit_position_data"] = {}
                context.user_data["require_market_position_data"] = None
                context.user_data["require_limit_position_data"] = None
            if context.user_data.get("limit_position_data") and "qty" in context.user_data.get("limit_position_data") and "price" in context.user_data.get("limit_position_data"):
                exchange_data = context.user_data.get("exchange")
                limit_position_data = context.user_data.get("limit_position_data")
                if exchange_data and limit_position_data and limit_position_data["qty"] and limit_position_data["price"]:
                    exchange = get_exchange_from_ccxt(exchange_data["exchange_id"], exchange_data["api_key"], exchange_data["secret_key"])
                    try :
                        await exchange.create_limit_sell_order(
                            symbol=symbol,
                            amount=limit_position_data["qty"],
                            price=limit_position_data["price"]
                        )
                        await update.effective_chat.send_message("Limit order was successfully created!")
                        await handle_positions_by_command(update, context)
                        await handle_orders_by_command(update, context)
                    except Exception as e:
                        print("error --> ", e)
                    finally :
                        await exchange.close()
                context.user_data["market_position_data"] = {}
                context.user_data["limit_position_data"] = {}
                context.user_data["require_market_position_data"] = None
                context.user_data["require_limit_position_data"] = None
    return CEX_ROUTES

async def handle_click_price_for_order_edit(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    order_id = context.user_data.get("select_order")
    orders = context.user_data.get("orders")
    if order_id in orders:
        order = orders[order_id]
        await update.effective_chat.send_message(f"Please enter the price in {order['unit']}.")
        context.user_data["require_edit_order_data"] = "price"
    return CEX_ROUTES

async def handle_click_amount_for_order_edit(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    order_id = context.user_data.get("select_order")
    orders = context.user_data.get("orders")
    if order_id in orders:
        order = orders[order_id]
        if order:
            await update.effective_chat.send_message(f"Please enter the amount in {order['ticker']}.")
            context.user_data["require_edit_order_data"] = "amount"
    return CEX_ROUTES

async def handle_limit_position(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat_id = update.callback_query.from_user.id
    positions = context.user_data.get("positions")
    symbol = update.callback_query.data.replace("position_limit_", "")
    if symbol in positions:
        position = positions[symbol]
        if not position:
            return CEX_ROUTES
        if chat_id in messages:
            if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
                messages[chat_id]["select_position"] = None
            if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
                messages[chat_id]["select_order"] = None
        limit_position_markup = InlineKeyboardMarkup([
            [
                InlineKeyboardButton(f"Price {position['unit']}", callback_data="input_position_price_for_limit"),
                InlineKeyboardButton(f"Qty {position['ticker']}", callback_data="input_position_qty_for_limit")
            ]
        ])
        select_position_message = await update.effective_message.reply_html(
            f"═ Create Limit Order (<code>{position['symbol']}</code>) ═\nEntry Price: <code>{position['price']}</code> \nQty: <code>{position['qty']}</code> \n\nSend /positionclose to keep the current position.",
            reply_markup=limit_position_markup
        )
        context.user_data["require_limit_position_data"] = None
        context.user_data["require_market_position_data"] = None
        context.user_data["require_edit_order_data"] = None
        context.user_data["select_position"] = {
            "symbol": symbol,
            "type": "limit"
        }
        if chat_id not in messages:
            messages[chat_id] = {}
        messages[chat_id]["select_position"] = select_position_message.message_id
    return CEX_ROUTES

async def handle_market_position(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat_id = update.callback_query.from_user.id
    positions = context.user_data.get("positions")
    symbol = update.callback_query.data.replace("position_market_", "")
    if chat_id in messages:
        if "select_position" in messages[chat_id] and messages[chat_id]["select_position"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_position"])
            finally :
                messages[chat_id]["select_position"] = None
        if "select_order" in messages[chat_id] and messages[chat_id]["select_order"]:
            try :
                await update.effective_chat.delete_message(message_id=messages[chat_id]["select_order"])
            finally :
                messages[chat_id]["select_order"] = None
    if symbol in positions:
        position = positions[symbol]
        if not position:
            return CEX_ROUTES
        context.user_data["select_position"] = {
            "type": "market",
            "symbol": symbol
        }
        market_position_markup = InlineKeyboardMarkup([
            [
                InlineKeyboardButton(f"Qty {position['ticker']}", callback_data="input_position_qty_for_market")
            ]
        ])
        select_position_message = await update.effective_message.reply_html(
            f"═ Create Market Order (<code>{position['symbol']}</code>) ═\nEntry Price: <code>{position['price']}</code> \nQty: <code>{position['qty']}</code> \n\nSend /positionclose to keep the current position.",
            reply_markup=market_position_markup
        )
        context.user_data["require_limit_position_data"] = None
        context.user_data["require_market_position_data"] = None
        context.user_data["require_edit_order_data"] = None
        if chat_id not in messages:
            messages[chat_id] = {}
        messages[chat_id]["select_position"] = select_position_message.message_id
    return CEX_ROUTES

async def handle_click_price_for_position_limit(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    positions = context.user_data.get("positions")
    select_position = context.user_data.get("select_position")
    if not positions or not select_position:
        return CEX_ROUTES
    if "symbol" in select_position:
        symbol = select_position["symbol"]
        if symbol in positions:
            position = positions[symbol]
            await update.effective_chat.send_message(f"Please enter the price in {position['unit']}.")
            context.user_data["require_limit_position_data"] = "price"
    return CEX_ROUTES

async def handle_click_qty_for_position_limit(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    positions = context.user_data.get("positions")
    select_position = context.user_data.get("select_position")
    if not positions or not select_position:
        return CEX_ROUTES
    if "symbol" in select_position:
        symbol = select_position["symbol"]
        if symbol in positions:
            position = positions[symbol]
            await update.effective_chat.send_message(f"Please enter the qty in {position['ticker']}")
            context.user_data["require_limit_position_data"] = "qty"
    return CEX_ROUTES

async def handle_click_qty_for_position_market(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    positions = context.user_data.get("positions")
    select_position = context.user_data.get("select_position")
    if not positions or not select_position:
        return CEX_ROUTES
    if "symbol" in select_position:
        symbol = select_position["symbol"]
        if symbol in positions:
            position = positions[symbol]
            await update.effective_chat.send_message(f"Please enter the qty in {position['ticker']}")
            context.user_data["require_market_position_data"] = "qty"
    return CEX_ROUTES

def run_socket() -> None:
    """Connect the socket server"""
    try :
        sio.connect("https://api.artemisdigital.io", transports=["websocket"])
        # await sio.wait()
    except Exception as e:
        print("error --> ", e)

def run_bot() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    # Setup conversation handler with the states FIRST and SECOND
    # Use the pattern parameter to pass CallbackQueries with specific
    # data pattern to the corresponding handlers.
    # ^ means "start of line/string"
    # $ means "end of line/string"
    # So ^ABC$ will only allow 'ABC'
    CEX_ROUTES_HANDLER_LIST = [
        CallbackQueryHandler(handle_connect_api_keys, pattern=f'^connect_{cex["slug"]}$') for cex in cex_list1 + cex_list2
    ]
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_connected_api_keys, pattern='handle_connected_api_keys'))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_api_keys_list_close, pattern="^api_keys_list_close$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_api_keys, pattern="^api_keys$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_show_connect_api_keys, pattern="^connect_api_keys$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(settings_menu_close, pattern="^settings_menu_close$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_cancel_connect_api_keys, pattern="^connect_api_keys_cancel$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_set_default_exchange, pattern="set_default_exchange"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_remove_api_key, pattern="disconnect_exchange"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(settings, pattern="^settings$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_api_keys, pattern="^connected_api_keys_close$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_buy_token, pattern="buy_token"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_sell_token, pattern="sell_token"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_positions, pattern="^__positions__$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_orders, pattern="^__orders__$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_cancel_order, pattern="order_cancel"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_edit_order, pattern="order_edit"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_click_price_for_order_edit, pattern="^input_order_price_for_edit$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_click_amount_for_order_edit, pattern="^input_order_amount_for_edit$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_limit_position, pattern="position_limit"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_market_position, pattern="position_market"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_click_price_for_position_limit, pattern="^input_position_price_for_limit$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_click_qty_for_position_limit, pattern="^input_position_qty_for_limit$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_click_qty_for_position_market, pattern="^input_position_qty_for_market$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_buy_limit_token, pattern="limit_buy"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_buy_market_token, pattern="market_buy"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_sell_limit_token, pattern="limit_sell"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_sell_market_token, pattern="market_sell"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_click_price_for_buy_limit, pattern="^input_buy_price_for_limit$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_click_qty_for_buy_limit, pattern="^input_buy_qty_for_limit$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_click_qty_for_buy_market, pattern="^input_buy_qty_for_market$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_click_price_for_sell_limit, pattern="^input_sell_price_for_limit$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_click_qty_for_sell_limit, pattern="^input_sell_qty_for_limit$"))
    CEX_ROUTES_HANDLER_LIST.append(CallbackQueryHandler(handle_click_qty_for_sell_market, pattern="^input_sell_qty_for_market$"))

    conv_handler = ConversationHandler(
        entry_points=[
            CommandHandler("start", start),
            CommandHandler("buyclose", handle_buy_close),
            CommandHandler("sellclose", handle_sell_close),
            CommandHandler("exchanges", handle_api_keys_by_command),
            CommandHandler("orders", handle_orders_by_command),
            CommandHandler("positions", handle_positions_by_command),
            CommandHandler("settings", settings_by_command),
            CommandHandler("orderclose", handle_order_close),
            CommandHandler("ordercancel", handle_order_cancel),
            CommandHandler("positionclose", handle_position_close)
        ],
        states={
            # START_ROUTES: [
            #     CallbackQueryHandler(settings, pattern="^settings$")
            # ],
            CEX_ROUTES: CEX_ROUTES_HANDLER_LIST,
            # END_ROUTES: [
            # ]
        },
        fallbacks=[
            CommandHandler("start", start),
            CommandHandler("exchanges", handle_api_keys_by_command),
            CommandHandler("orders", handle_orders_by_command),
            CommandHandler("positions", handle_positions_by_command),
            CommandHandler("settings", settings_by_command),
            CommandHandler("orderclose", handle_order_close),
            CommandHandler("ordercancel", handle_order_cancel),
            CommandHandler("positionclose", handle_position_close),
            CommandHandler("buyclose", handle_buy_close),
            CommandHandler("sellclose", handle_sell_close)
        ]
    )

    # Add ConversationHandler to application that will be used for handling updates
    application.add_handler(conv_handler)
    application.add_handler(MessageHandler(filters.REPLY, reply_handler))
    application.add_handler(MessageHandler(filters.CHAT, chat_handler))

    # Run the bot until the user presses Ctrl-C
    application.run_polling(allowed_updates=Update.ALL_TYPES)

def is_float_string(value):
    try :
        float(value)
        return True
    except :
        return False

def get_user_info_by_chat_id(chat_id: str):
    user_info = session.query(Users).join(UserSettingsGlobal).filter(UserSettingsGlobal.telegram_chat_id == chat_id).first()
    session.commit()
    return user_info

def get_api_keys_by_chat_id(chat_id: str):
    api_keys = session.query(ApiKey).join(Users).join(UserSettingsGlobal).filter(UserSettingsGlobal.telegram_chat_id == chat_id).all()
    session.commit()
    return api_keys

def get_api_key_by_id(id: str):
    api_key = session.query(ApiKey).filter(ApiKey.id == id).first()
    session.commit()
    return api_key

def delete_api_key_by_id(id: str):
    api_key_delete = session.query(ApiKey).filter_by(id=id).first()
    session.delete(api_key_delete)
    session.commit()
    return api_key_delete

def get_default_exchange_by_user_id(user_id: str):
    api_key = session.query(UserSettingsGlobal).filter_by(user_id=user_id).first()
    session.commit()
    return api_key.api_key

if __name__ == "__main__":
    run_socket()
    run_bot()