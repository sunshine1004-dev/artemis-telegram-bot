from sqlalchemy.dialects.mysql import LONGTEXT,TEXT
import random
from datetime import datetime, timezone
from sqlalchemy import DateTime
from sqlalchemy import Column, Table, Text, Integer, String, Boolean, Float, TIMESTAMP, DATETIME, ForeignKey, BIGINT, BigInteger, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, backref
import os
from dotenv import load_dotenv

load_dotenv()
DATABASE_URL = os.environ.get('DATABASE_URL')

engine = create_engine(DATABASE_URL)
Base = declarative_base()
Base.metadata.create_all(engine)

SessionLocal = sessionmaker(bind=engine)
session = SessionLocal()

# db = SQLAlchemy(session_options={"autoflush": False,"expire_on_commit": False})

def insert_many(session, instances):
    session.add_all(instances)
    session.commit()

def commit_db():
    session.commit()

def rollback_db():
    session.rollback()

class Source(Base):
    __tablename__ = 'source'
    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False)
    type = Column(String(30), nullable=False)
    img = Column(String(30), nullable=False)

class PushSubscription(Base):
    __tablename__ = "push_subscriptions"
    id = Column(Integer, primary_key=True)
    endpoint = Column(TEXT,unique=True)
    keys = Column(TEXT)
    def insert(self):
        session.add(self)
        commit_db()
        return 1

def generate_nonce(length=8):
  return ''.join([str(random.randint(0, 9)) for i in range(length)])

class UserWeb3(Base):
    __tablename__ = "user_web3"
    public_address = Column(String(80), primary_key=True, nullable=False, unique=True)
    nonce = Column(Integer(), nullable=False, default=generate_nonce)
    def insert(self):
        session.add(self)
        session.commit()

# class Discount(Base):
#     id = Column(Integer,primary_key=True)
#     code = Column(String(20))
#     discount = Column(Integer())
#     expired = Column(Boolean())

# class CourseLink(Base):
#     id = Column(Integer,primary_key=True)
#     time_created = Column(DateTime, default=datetime.utcnow)
#     time_clicked = Column(DateTime)
#     visits = Column(Integer)
#     token = Column(String(20))
#     url = Column(String(300))
#     country_code = Column(String(2))
#     ip = Column(String(20))

class CourseMCQ(Base):
    __tablename__ = "course_mcq"
    id = Column(Integer,primary_key=True)
    user_id = Column(String(40), nullable=True)
    submission_time = Column(DateTime, default=datetime.utcnow)
    course_id = Column(Integer)
    chapter = Column(Integer)
    result = Column(Float)


# class MailingSent(Base):
#     id = Column(Integer,primary_key=True)
#     email = Column(String(70))
#     time_sent = Column(DateTime, default=datetime.utcnow)
#     time_clicked = Column(DateTime)
#     opened = Column(Boolean())
#     clicked = Column(Boolean())
#     registered = Column(Boolean())
#     init_payment = Column(Boolean())
#     token = Column(String(20))
#     unsubscribed = Column(Boolean())
#     subject = Column(String(80))
#     template = Column(String(80))
#     country_code = Column(String(2))
#     level = Column(Integer)

#     def insert(self):
#         session.add(self)
#         session.commit()


class Mailing(Base):
    __tablename__ = "mailing_list"
    id = Column(Integer,primary_key=True)
    email = Column(String(70))
    date = Column(DateTime, default=datetime.utcnow)
    def insert(self):
        session.add(self)
        session.commit()


# class MailingLeak(Base):
#     id = Column(Integer,primary_key=True)
#     email = Column(String(70))
#     token =  Column(String(20))
#     source =  Column(String(20))
#     sent = Column(Integer())
#     unsubscribed = Column(Boolean())
#     country_code = Column(String(2))

#     def insert(self):
#         session.add(self)
#         session.commit()

class Users(Base):
    __tablename__ = 'users'
    public_id = Column(Integer,primary_key=True)
    email = Column(String(70))
    source = Column(String(10))
    password = Column(String(50))
    admin = Column(Boolean)
    confirmed = Column(Boolean, default=False,nullable=False)
    public_address = Column(String(80), nullable=False, unique=True)
    nonce = Column(Integer(), nullable=False, default=generate_nonce)
    subscription = Column(String(20))
    time_created = Column(DateTime, default=datetime.utcnow)
    course_1 = Column(Boolean(), default=False)
    course_2 = Column(Boolean(), default=False)
    course_3 = Column(Boolean(), default=False)
    trading = Column(Boolean(), default=False)
    register_country = Column(String(10))
    login_country = Column(String(10))
    register_ip = Column(String(15))
    login_ip = Column(String(15))
    discord_id = Column(String(20))

# class LeaderboardTrade(Base):
#     __tablename__ = 'leaderboard_trade'
#     id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
#     closed = Column(Boolean)
#     symbol = Column(String(20), nullable=False)
#     side = Column(String(6), nullable=False)
#     amount = Column(Float)
#     market_price = Column(Float)
#     entry_price = Column(Float)
#     leverage = Column(Integer)
#     roe = Column(Float)
#     user_id = Column(String(50), nullable=False)
#     user_name = Column(String(50), nullable=False)
#     pnl = Column(Integer)
#     image = Column(String(400))
#     created = Column(TIMESTAMP(timezone=False), nullable=False, default=None)
#     updated = Column(TIMESTAMP(timezone=False), nullable=False, default=None)
#     def __repr__(self):
#         return f'Trade <{self.user_name}> ({self.symbol})'

#     def insert(self):
#         print(f"Inserting {self}")
#         session.add(self)
#         session.commit()
#         return 1

class ApiKey(Base):
    __tablename__ = 'api_keys'
    id = Column(Integer, primary_key=True)
    user_id = Column(String(30), ForeignKey('users.public_id'), nullable=True)
    api_key = Column(String(82), nullable=False)
    api_passphrase = Column(String(82), nullable=False)
    secret_key = Column(String(82), nullable=False)
    exchange = Column(String(80), nullable=False)
    account_name = Column(String(82), nullable=True)
    app_id = Column(String(82), nullable=True)
    user = relationship('Users', backref='api_keys')

    def insert(self):
        session.add(self)
        session.commit()

# class Calendar(Base):
#     __tablename__ = 'calendar'
#     id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
#     title = Column(String(20), nullable=False)
#     description = Column(TEXT, nullable=False)
#     forecast = Column(Float)
#     previous = Column(Float)
#     unit = Column(String(2))
#     time = Column(DATETIME(timezone=True), nullable=False, default=None)
#     ric = Column(String(20), nullable=False)
#     investing_id = Column(String(20), nullable=False)

#     def __repr__(self):
#         return f'Calendar <{self.title}> ({self.time})'

#     def insert(self):
#         print(f"Inserting {self}")
#         session.add(self)
#         session.commit()
#         return 1


# class Autotrade(Base):
#     __tablename__ = 'autotrade'
#     id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
#     user_id = Column(String(30), nullable=True)
#     active = Column(Boolean)
#     exchange = Column(String(15), nullable=False) #TODO IF KEY DELETED, DELETE CASCADE
#     calendar_id = Column(Integer, ForeignKey('calendar.id'))
#     calendar = relationship("Calendar")
#     status = Column(String(20))
#     error = Column(TEXT)
#     success_order_id = Column(TEXT)
#     values = relationship('AutotradeValue', back_populates="autotrade",lazy='subquery') #https://stackoverflow.com/questions/13967093/parent-instance-is-not-bound-to-a-session-lazy-load-operation-of-attribute-acc

#     def __repr__(self):
#         return f'AutoTrade <{self.exchange}> ({self.calendar_id})'

#     def insert(self):
#         print(f"Inserting {self}")
#         session.add(self)
#         session.commit()
#         return 1


# class AutotradeValue(Base):
#     __tablename__ = 'autotrade_values'
#     id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
#     value = Column(Float, nullable=False)
#     operator = Column(String, nullable=False)
#     side = Column(String, nullable=False)
#     amount_usd = Column(Float, nullable=False)
#     leverage = Column(Integer, default=1)
#     autotrade_id = Column(Integer, ForeignKey('autotrade.id'))
#     autotrade = relationship('Autotrade',back_populates="values")


# # class LineChart(Base):
# #     id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
# #     signaltoken_id = Column('signaltoken_id', Integer, ForeignKey('signal_tokens.id'), index=True)
# #     price = Column(Float)
# #     order = Column(Integer)
# #     timestamp = Column(BIGINT)

# class ExchangeOhlcv(Base):
#     __tablename__ = 'exchange_ohlcv'
#     id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
#     pct_change = Column(Float)
#     high = Column(Float)
#     low = Column(Float)
#     open = Column(Float)
#     close = Column(Float)
#     volume = Column(Float)
#     pair = Column(String)
#     type = Column(String)
#     exchange = Column(String)
#     time = Column(BIGINT)
#     timeframe = Column(String)
#     notified = Column(Boolean)
#     time_found = Column(TIMESTAMP(timezone=False), nullable=False, default=None)

#     def insert(self):
#         session.add(self)
#         session.commit()
#         return 1


# class Signal(Base):
#     __tablename__ = 'signal'
#     id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
#     title = Column(LONGTEXT, nullable=False)
#     title_text = Column(LONGTEXT, nullable=False)
#     body = Column(LONGTEXT, nullable=False)
#     link = Column(String(200), nullable=False)
#     #tokens = relationship('Token', secondary=signal_tokens)
#     time = Column(TIMESTAMP(timezone=False), nullable=False, default=None)
#     timestamp = Column(BIGINT)
#     image = Column(String(300))
#     source = Column(String(50), nullable=False)
#     source_type = Column(String(50))
#     listing = Column(Boolean, default=False)
#     attachment = Column(TEXT)
#     is_nested = Column(Boolean, default=False)
#     nested_id = Column(Integer, ForeignKey('signal.id'), index=True)
#     nested = relationship('Signal', uselist=False)
#     retweet_ratio = Column(Float)
#     reply_ratio = Column(Float)
#     like_ratio = Column(Float)
#     quote_ratio = Column(Float)
#     mad_id = Column(String(40))
#     mad_likes =  Column(Integer,default=0)
#     mad_dislikes =  Column(Integer,default=0)
#     def __repr__(self):
#         return f'Signal <{self.title}> ({self.link})'

#     def insert(self):
#         print(f"Inserting signal {self}")
#         session.add(self)
#         session.commit()
#         return 1

# class TwitterTrack(Base):
#     __tablename__ = 'twitter_track'
#     id = Column(Integer, primary_key=True)
#     username = Column(String(30), nullable=False)
#     name = Column(TEXT, nullable=False)
#     image_url = Column(TEXT, nullable=False)
#     user_id = Column(BigInteger)
#     symbol = Column(String(20))
#     name = Column(TEXT, nullable=False)
#     category = Column(String(20))

#     def insert(self):
#         session.add(self)
#         session.commit()
#         print(f"Inserting {self}")

class UserSettingsGlobal(Base):
    __tablename__ = 'user_settings_global'
    id = Column(Integer, primary_key=True)
    user_id = Column(String(40), ForeignKey('users.public_id'))
    default_leverage = Column(Float)
    default_ticker = Column(String(15))
    telegram_chat_id = Column(String(60))
    default_exchange = Column(String(20), ForeignKey('api_keys.id'), nullable=True)
    user = relationship('Users', backref='settings')
    api_key = relationship('ApiKey', backref='settings')

    def insert(self):
        session.add(self)
        session.commit()
        print(f"Inserting {self}")

# class UserSettingsTwitter(Base):
#     __tablename__ = 'user_settings_twitter'
#     id = Column(Integer, primary_key=True)
#     user_id = Column(String(40))
#     twitter_id = Column(BigInteger)
#     #symbol = Column(String(20))
#     #name = Column(TEXT, nullable=False)
#     active = Column(Boolean, nullable=False)
#     sound = Column(Boolean, nullable=False)

#     def insert(self):
#         session.add(self)
#         session.commit()
#         print(f"Inserting {self}")




# # token_category_association = Table('token_category_association',
# #                             Column('token_id', Integer, ForeignKey('token.id'), primary_key=True),
# #                             Column('category_id', Integer, ForeignKey('token_category.id'), primary_key=True))

# # class TokenCategory(Base):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(60), unique=True, nullable=False)
# #     def to_dict(self):
# #         return {'id': self.id, 'name': self.name}


# # class Token(Base):
# #     __tablename__ = 'token'
# #     id = Column(Integer, primary_key=True)
# #     symbol = Column(String(120), nullable=False)
# #     name = Column(String(30), nullable=False)
# #     chain_id = Column('chain_id', Integer, default=0, index=True)
# #     coingecko_id = Column(String(60), nullable=False)
# #     small_img = Column(String(300), nullable=False)
# #     exchanges = relationship('ExchangePair')#back_populates="token"
# #     discord_url = Column(String(80))
# #     telegram_url = Column(String(80))
# #     twitter_url = Column(String(80))
# #     web_url = Column(String(200))
# #     twitter_name = Column(String(80))
# #     twitter_id = Column(Integer)
# #     announcement_id = Column(Integer)
# #     guild_id = Column(Integer)
# #     categories = relationship('TokenCategory', secondary=token_category_association, lazy='subquery',
# #                                  backref=backref('entries', lazy=True))
# #     market_cap = Column(Integer)
# #     total_supply = Column(Integer)
# #     circulating_supply = Column(Integer)
# #     cmc_id = Column(String(80))
# #     address = Column(String(80))
# #     description = Column(Text)


# #     def __repr__(self):
# #         return f'Token {self.name} ({self.symbol})'

# #     def insert(self):
# #         session.add(self)
# #         session.commit()
# #         print(f"Inserting {self}")

# # class TokenFavorite(Base):
# #     user_id = Column(String(40), primary_key=True, nullable=True)
# #     token_id = Column('token_id', Integer, ForeignKey('token.id'), primary_key=True)
# #     token = relationship(Token)
# #     ticker = Column(String(30), nullable=False)

# # class Notify(Base):
# #     id = Column(Integer, primary_key=True)
# #     source = Column(String(30), nullable=False)
# #     operator = Column(String(30), nullable=False)
# #     value = Column(String(30), nullable=False)
# #     alert = Column(String(30), nullable=False)
# #     label = Column(String(30), nullable=False)
# #     send = Column(String(30), nullable=False)
# #     send_id = Column(String(50), nullable=False)
# #     user_id = Column(String(30), ForeignKey('users.public_id'), index=True)
# #     user = relationship("Users")
# class SignalTokenTrades(Base):
#     __tablename__ = 'signal_token_trades'
#     id = Column(Integer, primary_key=True)
#     #signaltoken_id = Column('signaltoken_id', Integer, ForeignKey('signal_tokens.id'), index=True)
#     signaltoken_id = Column('signaltoken_id', Integer, ForeignKey('signal_tokens.id'), index=True)
#     exchange = Column(String(20), nullable=False)
#     timestamp = Column(DateTime, nullable=False)
#     time_diff = Column(Float, nullable=False)
#     order_value = Column(Float, nullable=False)
#     price = Column(Float, nullable=False)
#     side = Column(String(10), nullable=False)
#     signal_id = Column(Integer, nullable=False)
#     #signaltoken_id = Column(Integer, nullable=False)
#     pair = Column(String(15), nullable=False)

#     def insert(self):
#         session.add(self)
#         session.commit()
#         return 1

# # class SignalTokens(Base):
# #     __tablename__ = 'signal_tokens'
# #     id = Column('id', Integer, primary_key=True)
# #     signal_id = Column('signal_id', Integer, ForeignKey('signal.id'), index=True)
# #     token_id = Column('token_id', Integer, ForeignKey('token.id'), index=True)
# #     signal = relationship(Signal, backref="signaltokens")
# #     token = relationship(Token, backref="signaltokens")
# #     price_found = Column(Float)
# #     change_1m = Column(Float) #Remove me ?
# #     change_5m = Column(Float) #Remove me ?
# #     change_1h = Column(Float) #Remove me ?
# #     exchange_name = Column(String(15))
# #     market_id = Column(String(20))
# #     market_base = Column(String(15))
# #     market_quote = Column(String(15))
# #     type = Column(String(15))
# #     is_future = Column(Boolean)
# #     is_spot = Column(Boolean)
# #     link = Column(Text)
# #     line_chart = relationship(LineChart)
# #     trades = relationship('SignalTokenTrades', backref='signaltoken')
# #     #trades = relationship(SignalTokenTrades)
# #     def insert(self):
# #         session.add(self)
# #         session.commit()
# #         return 1

# class Product(Base):
#     __tablename__ = 'product'
#     id = Column(Integer, primary_key=True)
#     name = Column(String(50))
#     price_usd = Column(Float(), nullable=False)
#     tva = Column(Float(), nullable=False)

# class Invoice(Base):
#     __tablename__ = 'invoice'
#     id = Column(Integer, primary_key=True)
#     status = Column(String(11))
#     refno = Column(String(30), unique=True)
#     product_id = Column(Integer, ForeignKey('product.id'), index=True)
#     transaction_id = Column(Integer)
#     product = relationship("Product")
#     amount = Column(Float(), nullable=False)
#     currency = Column(String(10), nullable=False)
#     user_id = Column(String(30), ForeignKey('users.public_id'), index=True)
#     user = relationship("Users")
#     time_created = Column(DateTime, default=datetime.utcnow)
#     recurring = Column(Boolean)
#     remaining = Column(Integer)
#     alias = Column(String(120))
#     expiry_month = Column(String(3))
#     expiry_year = Column(String(3))
#     payment_method = Column(String(20))
#     promo_code =  Column(String(20))
#     tracking_token = Column(String(20))
#     country = Column(String(5))

#     def insert(self):
#         session.add(self)
#         session.commit()

# # class Glossary(Base):
# #     id = Column(Integer, primary_key=True)
# #     name = Column(String(60), nullable=False)
# #     content = Column(Text)
# #     def to_dict(self):
# #         return {
# #             'id': self.id,
# #             'name': self.name,
# #             'content': self.content,
# #         }


# entry_categories = Table('tool_entry_categories',
#                             Column('entry_id', Integer, ForeignKey('tool_entry.id'), primary_key=True),
#                             Column('category_id', Integer, ForeignKey('tool_category.id'), primary_key=True)
#                         )
# class ToolEntry(Base):
#     id = Column(Integer, primary_key=True)
#     addition_time = Column(DateTime, default=datetime.utcnow)
#     name = Column(String(100), nullable=False)
#     link = Column(String(255), nullable=False)
#     favicon = Column(String(255))
#     quality_tier = Column(String(50))
#     network = Column(String(50))
#     screenshot = Column(String(255))
#     description = Column(Text)
#     categories = relationship('ToolCategory', secondary=entry_categories, lazy='subquery',
#                                  backref=backref('entries', lazy=True))

#     def to_dict(self):
#         return {
#             'id': self.id,
#             'addition_time': self.addition_time.strftime('%m/%d/%Y') if self.addition_time else None,
#             'name': self.name,
#             'link': self.link,
#             'quality_tier': self.quality_tier,
#             'network': self.network,
#             'img': self.favicon,
#             'description': self.description,
#             'screenshot': f"https://www.artemisdigital.io/{self.screenshot}" if len(self.screenshot) else "",
#             'categories': [category.to_dict() for category in self.categories]
#         }


# class ToolCategory(Base):
#     id = Column(Integer, primary_key=True)
#     name = Column(String(50), unique=True, nullable=False)

#     def to_dict(self):
#         return {'id': self.id, 'name': self.name}

# class VatCountry(Base):
#     id = Column(Integer, primary_key=True)
#     country_code = Column(String(2), nullable=False)
#     country_name = Column(String(100), nullable=False)
#     standard_rate = Column(Float, nullable=False)

# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################
# ################################################################

# class Wallet(Base):
#     __tablename__ = "zzz_wallet"
#     address = Column(String(43), primary_key=True)
#     ens = Column(String(300))
#     chain_id = Column('chain_id', Integer, ForeignKey('zzz_chain.blockchain_id'), default=0, index=True)
#     chain = relationship("Chain")


# class Watchlist(Base):
#     __tablename__ = "zzz_watchlist"
#     id = Column(Integer, primary_key=True)
#     name = Column(String(50))
#     watches = relationship("Watch")
#     def insert(self):
#         session.add(self)
#         session.commit()

# class Watch(Base):
#     __tablename__ = "zzz_watch"
#     id = Column(Integer, primary_key=True)
#     watchlist_id = Column("watchlist_id",Integer, ForeignKey('zzz_watchlist.id'), index=True)
#     tx_from = Column(String(80), ForeignKey('zzz_wallet.address'), nullable=False)
#     tx_from_wallet = relationship('Wallet')

# class Method(Base):
#     __tablename__ = "zzz_method"
#     id = Column(String(8), primary_key=True)
#     name = Column(String(300))



# class ExchangePair(Base):
#     __tablename__ = 'zzz_exchange_pair'
#     id = Column(Integer, primary_key=True)
#     token_id = Column(Integer, ForeignKey('token.id'))
#     token = relationship("Token",back_populates="exchanges")
#     exchange_id = Column(Integer, ForeignKey('zzz_exchange.id'))
#     exchange = relationship("Exchange")
#     trade_url = Column(String(200), default="")
#     volume = Column(Integer, default=0)

#     def insert(self):
#         session.add(self)
#         session.commit()

# class Chain(Base):
#     __tablename__ = 'zzz_chain'
#     id = Column(Integer, primary_key=True)
#     coingecko_id = Column(String(30), nullable=False)
#     name = Column(String(30), nullable=False)
#     img = Column(String(100), default=" ")
#     explorer = Column(String(100))
#     explorer_img = Column(String(50))
#     provider = Column(String(250))
#     blockchain_id = Column(Integer)
#     order = Column(Integer)
#     def insert(self):
#         session.add(self)
#         session.commit()
#         print(f"Inserting {self}")

# class Exchange(Base):
#     __tablename__ = 'zzz_exchange'
#     id = Column(Integer, primary_key=True)
#     coingecko_id = Column(String(30), nullable=False)
#     name = Column(String(30), nullable=False)
#     img = Column(String(100), nullable=False)
#     url = Column(String(60), nullable=False)
#     chain_id = Column('chain_id', Integer, ForeignKey('zzz_chain.blockchain_id'), default=0, primary_key=True)
#     chain = relationship("Chain")
#     is_dex = Column(Boolean, default=0)
#     LP_token_name = Column(String(40))
#     LP_token_symbol = Column(String(15))
#     def insert(self):
#         session.add(self)
#         session.commit()
#         print(f"Inserting {self}")

# class Pair(Base):
#     __tablename__ = 'zzz_pair'
#     pool = Column(String(80),ForeignKey('zzz_contract.address'), primary_key=True)
#     chain_id = Column('chain_id', Integer, ForeignKey('zzz_chain.blockchain_id'),default=0, primary_key=True)
#     chain = relationship("Chain")
#     token1 = Column(String(80), ForeignKey('zzz_contract.address'), index=True)
#     token1_obj = relationship("Contract", foreign_keys=[token1, chain_id])
#     token1_locked = Column(Float)
#     token2 = Column(String(80), ForeignKey('zzz_contract.address'), index=True)
#     token2_obj = relationship("Contract", foreign_keys=[token2, chain_id])
#     token2_locked = Column(Float)
#     time_found = Column(TIMESTAMP(timezone=False), nullable=False, default=None)
#     total_locked_usd = Column(Integer, default=0)
#     price = Column(Float)
#     price_usd = Column(Float)
#     price_change_5m = Column(Float)
#     price_change_15m = Column(Float)
#     price_change_30m = Column(Float)
#     price_change_1h = Column(Float)
#     price_change_3h = Column(Float)
#     price_change_6h = Column(Float)
#     price_change_12h = Column(Float)
#     price_change_1d = Column(Float)
#     price_change_3d = Column(Float)
#     price_change_7d = Column(Float)
#     buy_volume_1h = Column(Integer)
#     buy_volume_1d = Column(Integer)
#     sell_volume_1h = Column(Integer)
#     sell_volume_1d = Column(Integer)

#     exchange_id = Column(String(80), ForeignKey('zzz_exchange.id'), index=True)
#     exchange = relationship("Exchange")
#     def insert(self):
#         self.time_found = datetime.now(timezone.utc)
#         session.add(self)
#         #commit_db()
#         return 1

# class Contract(Base):
#     __tablename__ = 'zzz_contract'
#     address = Column(String(80),ForeignKey(Pair.pool), primary_key=True)
#     chain_id = Column('chain_id', Integer, ForeignKey('zzz_chain.blockchain_id'),default=0, primary_key=True)
#     chain = relationship("Chain")
#     pair = relationship("Pair", uselist=False,foreign_keys=[address,chain_id])
#     time_found = Column(TIMESTAMP(timezone=False), nullable=False, default=None)
#     no_token = Column(Boolean)
#     is_pool = Column(Boolean)
#     name = Column(TEXT)
#     symbol = Column(TEXT)
#     decimals = Column(Integer)
#     coingecko_id = Column(String(40),default="")
#     traded = Column(Integer)
#     alt_token_id = Column(String(80), ForeignKey('zzz_contract.address'), index=True)
#     alt_token = relationship("Contract",remote_side=[address])
#     exchange_id = Column(String(80), ForeignKey('zzz_exchange.id'), index=True)
#     exchange = relationship("Exchange")
#     image = Column(String(80))
#     def __repr__(self):
#         return f'Token {self.name} ({self.symbol}) : {self.address}'

#     def get_img(self):
#         if self.exchange is not None:
#             return self.exchange.img
#         elif self.alt_token is not None:
#             return self.alt_token.image
#         return ""

#     def insert(self):
#         try:
#             if self.time_found is None and self.traded == 1:
#                 self.time_found = datetime.now()
#             session.add(self)
#             session.commit()
#             print(f"Inserting {self}")
#         except Exception as e:
#             print(self)
#             raise e

# class BlockTime(Base):
#     __tablename__ = 'zzz_block_time'
#     block = Column(Integer, primary_key=True)
#     chain_id = Column('chain_id', Integer, ForeignKey('zzz_chain.blockchain_id'), default=0, index=True)
#     chain = relationship("Chain")
#     time = Column(TIMESTAMP(timezone=False), nullable=False, default=None)
#     def insert(self):
#         ret = BlockTime.query.filter_by(block=self.block).first()
#         if ret is not None:
#             return 0
#         session.add(self)

#         return 1


# class Swap(Base):
#     __tablename__ = 'zzz_swap'
#     hash = Column(String(80),ForeignKey('zzz_transaction.hash'), primary_key=True)
#     index = Column(Integer, nullable=False)
#     chain_id = Column('chain_id', Integer, ForeignKey('zzz_chain.blockchain_id'), default=0, primary_key=True)
#     chain = relationship("Chain")
#     transaction = relationship('Transaction',back_populates="swaps",foreign_keys=[hash, chain_id])
#     pair = Column(String(80), ForeignKey('zzz_pair.pool'), index=True)
#     pair_obj = relationship("Pair", foreign_keys=[pair, chain_id])

#     token_in = Column(String(80), ForeignKey('zzz_contract.address'), index=True)
#     token_in_obj = relationship("Contract", foreign_keys=[token_in, chain_id])
#     token_out = Column(String(80), ForeignKey('zzz_contract.address'), index=True)
#     token_out_obj = relationship("Contract", foreign_keys=[token_out, chain_id])
#     amount_in = Column(Float)
#     amount_out = Column(Float)
#     price = Column(Float)
#     price_usd1 = Column(Float)
#     price_usd2 = Column(Float)
#     usd_worth = Column(Float, default=0)
#     liquidity_total_usd = Column(Integer, default=0)
#     def insert(self):
#         session.add(self)
#         #commit_db()
#         return 1

# class Transaction(Base):
#     __tablename__ = 'zzz_transaction'
#     hash = Column(String(80), primary_key=True)
#     chain_id = Column('chain_id', Integer, ForeignKey('zzz_chain.blockchain_id'), default=0, primary_key=True)
#     chain = relationship("Chain")
#     block = Column(Integer, ForeignKey('zzz_block_time.block'),nullable=False)
#     blocktime = relationship('BlockTime',foreign_keys=[block, chain_id])
#     index = Column(Integer, nullable=False)
#     gas = Column(Integer, nullable=False)
#     tx_from = Column(String(200), ForeignKey('zzz_wallet.address'), nullable=False)
#     tx_from_wallet = relationship('Wallet')
#     tx_to = Column(String(80), ForeignKey('zzz_contract.address'), index=True,nullable=False)
#     tx_to_label = relationship('Contract',foreign_keys=[tx_to, chain_id])
#     method_id = Column(String(8), ForeignKey('zzz_method.id'))
#     method = relationship('Method')
#     pair = Column(String(80), ForeignKey('zzz_pair.pool'), index=True)
#     pair_obj = relationship("Pair", foreign_keys=[pair, chain_id])
#     tag = Column(Integer, default=0)
#     token_in = Column(String(80), ForeignKey('zzz_contract.address'), index=True)
#     token_in_obj = relationship("Contract", foreign_keys=[token_in, chain_id])
#     token_out = Column(String(80), ForeignKey('zzz_contract.address'), index=True)
#     token_out_obj = relationship("Contract", foreign_keys=[token_out, chain_id])
#     token_mid = Column(String(80), ForeignKey('zzz_contract.address'), index=True)
#     token_mid_obj = relationship("Contract", foreign_keys=[token_mid, chain_id])
#     amount_in = Column(Float)
#     amount_out = Column(Float)
#     amount_mid = Column(Float)
#     usd_worth = Column(Integer, default=0)
#     swaps = relationship('Swap',back_populates="transaction")


#     def __repr__(self):
#         return f'Transaction <{self.hash}>'

#     def insert(self):
#         session.add(self)
#         return 1

#     def delete(self):
#         session.delete(self)

# class BaseToken(Base):
#     __tablename__ = 'zzz_base_token'
#     symbol = Column(String(12), ForeignKey('zzz_contract.symbol'), primary_key=True,index=True)
#     chain_id = Column(Integer,index=True)
#     stable = Column(Boolean)
#     uni_price_address = Column(String(80), index=True)
#     contracts = relationship("Contract",uselist=True)
#     binance_ticker = Column(String(20))
#     priority = Column(Integer)
#     def __repr__(self):
#         return f'Event <{self.hash}>'

#     def insert(self):
#         session.add(self)
#         return 1

#     def delete(self):
#         session.delete(self)


# class DailyApeLink(Base):
#     __tablename__ = "zzz_dailyape_link"
#     id = Column(Integer, primary_key=True)
#     link = Column(String(400))
#     ape_message_id = Column('ape_message_id', Integer, ForeignKey('zzz_dailyape_message.id'), index=True)
#     preview_title = Column(String(400))
#     preview_description = Column(String(600))
#     preview_image = Column(String(400))
#     twitter_thread = Column(Text)
#     #ape_message = relationship('DailyApeMessage')

# class DailyApeMessage(Base):
#     __tablename__ = "zzz_dailyape_message"
#     id = Column(Integer, primary_key=True)
#     date = Column(DateTime, nullable=False)
#     name = Column(String(300))
#     category = Column(String(100))
#     links = relationship('DailyApeLink')

#     def insert(self):
#         session.add(self)
#         session.commit()









