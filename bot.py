#!/usr/bin/env python
"""Simple Bot to reply to Telegram messages.

This is built on the API wrapper, see echobot.py to see the same example built
on the telegram.ext bot framework.
This program is dedicated to the public domain under the CC0 license.
"""
import os
import asyncio
from dotenv import load_dotenv
import contextlib
import logging
from typing import NoReturn

from utils.models import Users, UserSettingsGlobal, ApiKey, SessionLocal

from telegram import (
    Bot,
    Update,
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    ForceReply,
    Chat,
    ChatMember,
    ChatMemberUpdated
)
from telegram.error import Forbidden, NetworkError
from telegram.constants import ParseMode
from telegram.ext import (
    Application,
    ChatMemberHandler,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    CallbackQueryHandler,
    MessageHandler,
    filters,
)

load_dotenv()
BOT_TOKEN = os.environ.get('TELE_BOT_TOKEN')
session = SessionLocal()

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)

cex_list1 = [
    {
        "title": "Binance",
        "slug": "binance"
    },
    {
        "title": "Kucoinfutures",
        "slug": "kucoinfutures"
    },
    {
        "title": "Bybit",
        "slug": "bybit"
    },
]

cex_list2 = [
    {
        "title": "Bitget",
        "slug": "bitget"
    },
    {
        "title": "GateIO",
        "slug": "gateio"
    },
    {
        "title": "OKX",
        "slug": "okx"
    },
]

START_ROUTES, END_ROUTES = range(2)

async def start_private_chat(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Greets the user and records that they started a chat with the bot if it's a private chat.
    Since no `my_chat_member` update is issued when a user starts a private chat with the bot
    for the first time, we have to track it explicitly here.
    """
    user_name = update.effective_user.full_name
    # chat = update.effective_chat
    start_reply_markup = InlineKeyboardMarkup([
        [InlineKeyboardButton('Buy', callback_data='buy'), InlineKeyboardButton('Sell', callback_data='sell')],
        [InlineKeyboardButton('Settings', callback_data='settings')]
    ])

    # if chat.type != Chat.PRIVATE or chat.id in context.bot_data.get("user_ids", set()):
    #     return

    user_info = get_user_info_by_chat_id(update.message.from_user.id)
    logger.info("%s started a private chat with the bot", user_name)
    # context.bot_data.setdefault("user_ids", set()).add(chat.id)
    if user_info:
        await update.message.reply_text(
            f"Exciting news! $COOKIE is launching on Bybit exchange on June 13th, 2024!",
            reply_markup=start_reply_markup
        )
    else :
        await update.message.reply_html(f'Please connect your chat id to our website. Your telegram chat id is <code>{update.message.from_user.id}</code> 👉 <a href="https://app.artemisdigital.io/settings/trading">Connect</a>')

async def settings(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Show new choice of buttons"""
    # query = update.callback_query
    # # await query.answer()
    # keyboard = [
    #     [
    #         InlineKeyboardButton("3", callback_data=str(THREE)),
    #         InlineKeyboardButton("4", callback_data=str(FOUR)),
    #     ]
    # ]
    # reply_markup = InlineKeyboardMarkup(keyboard)
    # # await query.edit_message_text(
    # #     text="First CallbackQueryHandler, Choose a route", reply_markup=reply_markup
    # # )
    # await query.edit_message_text(
    #     text="First CallbackQueryHandler, Choose a route", reply_markup=reply_markup
    # )
    print("settings")
    await update.effective_chat.send_message("═ ⚙️ Settings ═")
    return START_ROUTES

def main() -> None:
    """Run the bot."""
    application = Application.builder().token(BOT_TOKEN).build()
    # application.add_handler(MessageHandler(filters.ALL, start_private_chat))
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start_private_chat)],
        states={
            START_ROUTES: [
                # CallbackQueryHandler(buy, pattern="^buy$"),
                # CallbackQueryHandler(two, pattern="^sell$"),
                CallbackQueryHandler(settings, pattern="^buy$")
            ],
        },
        fallbacks=[CommandHandler("start", start_private_chat)]
    )
    application.add_handler(conv_handler)
    application.run_polling(allowed_updates=Update.ALL_TYPES)
    # Here we use the `async with` syntax to properly initialize and shutdown resources.
    # async with Bot(BOT_TOKEN) as bot:
    #     # get the first pending update_id, this is so we can skip over it in case
    #     # we get a "Forbidden" exception.
    #     try:
    #         update_id = (await bot.get_updates())[0].update_id
    #     except IndexError:
    #         update_id = None

    #     logger.info("listening for new messages...")
    #     while True:
    #         try:
    #             update_id = await echo(bot, update_id)
    #         except NetworkError:
    #             await asyncio.sleep(1)
    #         except Forbidden:
    #             # The user has removed or blocked the bot.
    #             update_id += 1


async def echo(bot: Bot, update_id: int) -> int:
    """Echo the message the user sent."""
    global cex_list1
    global cex_list2
    # Request updates after the last update_id
    updates = await bot.get_updates(offset=update_id, timeout=10, allowed_updates=Update.ALL_TYPES)
    keyboard = [
        [InlineKeyboardButton('Buy', callback_data='buy'), InlineKeyboardButton('Sell', callback_data='sell')],
        [InlineKeyboardButton('Settings', callback_data='settings')]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    settings_keyboard = [
        [InlineKeyboardButton('Menu', callback_data='menu'), InlineKeyboardButton('Close', callback_data='close')],
        [InlineKeyboardButton('API keys list', callback_data='api_keys')]
    ]
    settings_reply_markup = InlineKeyboardMarkup(settings_keyboard)
    connect_api_keyboard = [
        [InlineKeyboardButton('Connect your CEX API keys', callback_data='connect_api_keys')],
    ]
    connect_api_reply_markup = InlineKeyboardMarkup(connect_api_keyboard)
    connect_cex_list = [
        [InlineKeyboardButton(cex["title"], callback_data=cex["slug"]) for cex in cex_list1],
        [InlineKeyboardButton(cex["title"], callback_data=cex["slug"]) for cex in cex_list2]
    ]
    connect_cex_list_keyboard = InlineKeyboardMarkup(connect_cex_list)

    for update in updates:
        next_update_id = update.update_id + 1

        # your bot can receive updates without messages
        # and not all messages contain text
        if update.callback_query and update.callback_query.data:
            if update.callback_query.data == "buy":
                await bot.send_message(update.callback_query.from_user.id, text="✏️ Enter the token amount you want to buy:", reply_markup=ForceReply(selective=True))
            # if update.callback_query.data == "sell":
            #     await bot.send_message(update.callback_query.from_user.id, text="✏️ Enter the token amount you want to sell:", reply_markup=ForceReply(selective=True))
            if update.callback_query.data == "settings":
                await bot.send_message(update.callback_query.from_user.id, text="═ ⚙️ Settings ═", reply_markup=settings_reply_markup)
            if update.callback_query.data == "close":
                await bot.delete_message(update.callback_query.from_user.id, update.callback_query.message.message_id)
            if update.callback_query.data == "api_keys_list_close":
                await bot.delete_message(update.callback_query.from_user.id, update.callback_query.message.message_id)
            if update.callback_query.data == "menu":
                await bot.delete_message(update.callback_query.from_user.id, update.callback_query.message.message_id)
            if update.callback_query.data == "connect_api_keys":
                await update.callback_query.edit_message_text('You can connect the following CEX platforms.', reply_markup=connect_cex_list_keyboard)
            if update.callback_query.data == "api_keys":
                chat_id = update.callback_query.from_user.id
                api_keys = get_api_keys_by_chat_id(chat_id)
                if api_keys:
                    api_key_list_keyboard = [
                        [InlineKeyboardButton(api.exchange.replace('_', ' ').title(), callback_data=api.exchange) for api in api_keys[key:key + 2]] for key in range(0, len(api_keys), 2)
                    ]
                    api_key_list_keyboard.append([InlineKeyboardButton('Close', callback_data='api_keys_list_close')])
                    api_key_list_markup = InlineKeyboardMarkup(api_key_list_keyboard)
                    await update.callback_query.edit_message_text('List of API keys already connected', reply_markup=api_key_list_markup)
                else :
                    await update.callback_query.edit_message_text('There is no any connected API keys.', reply_markup=connect_api_reply_markup)
            for cex in cex_list1 + cex_list2:
                if update.callback_query.data == cex["slug"]:
                    await bot.send_message(update.callback_query.from_user.id, text=f"Connect your account\n Input your {cex["title"]} account name", reply_markup=ForceReply(selective=True))
                    await bot.delete_message(update.callback_query.from_user.id, update.callback_query.message.message_id)
            # if update.callback_query.data == "binance":
            #     await update.callback_query.edit_message_text("Connect your Binance account.")

        if update.message and update.message.text:
            if update.message.reply_to_message:
                print(update.message.reply_to_message.text)
            #     token_amount = update.message.text
            #     if token_amount.isdigit():
            #         await bot.send_message(update.message.from_user.id, text=f"You're buying {token_amount} COOKIE")
            #     else :
            #         await bot.delete_message(update.message.from_user.id, update.message.message_id)
            #         await bot.send_message(update.message.from_user.id, text="Invalid token amount. Enter the token amount you want to buy:", reply_markup=ForceReply(selective=True))
            #     return next_update_id
            # Reply to the message
            # chat_id = update.message.from_user.id
            user_info = get_user_info_by_chat_id(update.message.from_user.id)
            logger.info("Found message %s!", update.message.text)
            if user_info:
                await update.message.reply_text('Exciting news! $COOKIE is launching on Bybit exchange on June 13th, 2024!', reply_markup=reply_markup)
            else :
                await update.message.reply_html(f'Please connect your chat id to our website. Your telegram chat id is <code>{update.message.from_user.id}</code> 👉 <a href="https://app.artemisdigital.io/settings/trading">Connect</a>')
        return next_update_id
    return update_id

def get_user_info_by_chat_id(chat_id: str):
    user_info = session.query(Users).join(UserSettingsGlobal).filter(UserSettingsGlobal.telegram_chat_id == chat_id).first()
    return user_info

def get_api_keys_by_chat_id(chat_id: str):
    api_keys = session.query(ApiKey).join(Users).join(UserSettingsGlobal).filter(UserSettingsGlobal.telegram_chat_id == chat_id).all()
    return api_keys

if __name__ == "__main__":
    with contextlib.suppress(KeyboardInterrupt):  # Ignore exception when Ctrl-C is pressed
        asyncio.run(main())